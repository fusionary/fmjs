module.exports = {
  extends: 'kswedberg',
  rules: {
    indent: [
      'warn',
      2,
      {
        outerIIFEBody: 0
      }
    ]
  }
};
