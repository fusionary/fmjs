module.exports = function(grunt) {
  'use strict';
  var matchdep = require('matchdep');
  var path = require('path');
  var fs = require('fs');
  var cwd = process.cwd();
  var webpackConfig = require('./webpack');
  // Load all npm tasks from package.json

  matchdep.filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  var copyjQueryConfig = {
    expand: true,
    cwd: 'src/',
    src: 'jquery*.js',
    dest: './',
    flatten: true,
    filter: 'isFile',
    options: {}
  };

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
      ' Licensed <%= pkg.license %> */\n',
    modules: {

      'jquery.accordiontabs.js': ['jquery', 'jquery-ui/ui/tabs'],
      'jquery.anyajax.js': ['jquery'],
      'jquery.formfield.js': ['jquery'],
      'jquery.imagesloaded.js': ['jquery'],
      'jquery.inlinesvg.js': ['jquery'],
      'jquery.modal.js': ['jquery'],
      'jquery.responsive-images.js': ['jquery'],
      'jquery.social.js': ['jquery'],
      'jquery.textsizer.js': ['jquery'],
      'jquery.cachedajax.js': ['jquery', './url'],
    },
    // Task configuration.
    concat: {
      options: {
        banner: '<%= banner %>',
        stripBanners: true
      },
      fm: {
        src: 'dist/fm.js',
        dest: 'dist/fm.js'
      },
      min: {
        src: 'dist/fm.min.js',
        dest: 'dist/fm.min.js'
      }

    },
    uglify: {
      options: {
        banner: '<%= banner %>'
      },
      fm: {
        src: '<%= concat.fm.dest %>',
        dest: 'dist/fm.min.js'
      }
    },
    eslint: {
      options: {
        configFile: '.eslintrc.js',
      },
      gruntfile: {
        src: ['Gruntfile.js']
      },
      fm: {
        src: ['src/*.js', '!src/jquery*.js']
      },
      plugins: {
        src: ['src/jquery*.js']
      }
    },
    qunit: {
      es6: {
        options: {
          urls: [
            'http://localhost:8001/test/index.html'
          ]
        }
      },
      dist: {
        options: {
          urls: [
            'http://localhost:8001/test/dist/index.html'
          ]
        }
      }
    },
    connect: {
      server: {
        options: {
          port: 8001,
          base: '.'
        }
      }
    },
    version: {
      a: {
        src: ['package.json']
      },
      dist: {
        options: {
          prefix: '- v'
        },
        src: ['dist/fm.js', 'dist/fm.min.js']
      }
    },
    watch: {
      gruntfile: {
        files: '<%= eslint.gruntfile.src %>',
        tasks: ['eslint:gruntfile']
      },
      fm: {
        files: ['<%= eslint.fm.src %>', 'test/test.js'],
        tasks: ['test']
      },
      // test: {
      //   files: ['test/es6/test.js'],
      //   tasks: ['connect', 'qunit:es6']
      // },
      plugins: {
        files: '<%= eslint.plugins.src %>',
        tasks: ['eslint:plugins']
      }
    },
    webpack: webpackConfig,
    copy: {
      fm: {
        expand: true,
        cwd: 'src/',
        src: ['*.js', '!jquery*.js'],
        dest: './',
        flatten: true,
        filter: 'isFile',
      },
      jquery: Object.assign({}, copyjQueryConfig, {
        options: {
          process: function(content, filepath) {
            var file = path.basename(filepath);
            var modules = grunt.config('modules');
            var deps = modules[file];
            var depString = `'${deps.join('\', \'')}'`;
            var options = {
              data: {
                dependencies: depString
              }
            };

            content = content.replace(/(import )jQuery/, '$1 $');
            content = content.replace(/(\(function\(\$\) \{)/, '');
            content = content.replace(/(\}\))\(jQuery\);?/, '');

            return content;
          }
        }
      }),
      jqueryAmd: Object.assign({}, copyjQueryConfig, {
        src: ['jquery*.js', '!jquery.cachedajax.js'],
        dest: './amd/',
        options: {
          process: function(content, filepath) {
            var file = path.basename(filepath);
            var modules = grunt.config('modules');
            var deps = modules[file];
            var depString = `'${deps.join('\', \'')}'`;
            var options = {
              data: {
                dependencies: depString
              }
            };

            var jqIntroTemplate = grunt.file.read('./templates/jquery-intro.tpl');
            var intro = grunt.template.process(jqIntroTemplate, options);

            content = content.replace(/^\s*import.+;$/gm, '');
            content = content.replace(/(\(function\(\$\) \{)/, intro);
            content = content.replace(/(\}\))\(jQuery\)/, '$1)');

            return content;
          }
        }
      }),
      testDist: {
        src: 'test/test.js',
        dest: 'test/dist.js',
        options: {
          process: function(content, filepath) {
            content = content
            .replace(/import[^;]+;\n*/g, '')
            .replace('\'use strict\';', 'var extend = FM.extend; var getProperty = FM.getProperty;');

            content = content.replace(/test\/index/g, 'test/dist/index');

            [
              '(arrays)\\.',
              '(maths)\\.',
              '(strings)\\.',
              '(urls)\\.',
              '(events)\\.',
              '(addElements)\\.',
              '(youtubes)\\.',
              '(detects)\\.',
              '(selections)\\.',
            ].forEach(function(item) {
              var rItem = new RegExp(item, 'g');

              content = content.replace(rItem, 'FM.');
            });

            return content;
          }
        }
      }
    }
  });

  // Running `grunt prepub` in npm prepublish script, because that's really the only time we need to.
  // All the files copied by it are unsourced.
  grunt.registerTask('prepub', ['copy:fm', 'copy:jquery', 'copy:jqueryAmd']);

  grunt.registerTask('test', ['webpack:nodetest', 'eslint', 'connect', 'qunit:es6', 'webpack:browsertest']);
  grunt.registerTask('testDist', ['copy:testDist', 'webpack:dist', 'webpack:testDist', 'connect', 'qunit:dist']);

  grunt.registerTask('build', ['test', 'webpack:dist', 'webpack:min', 'concat']);

  ['patch', 'minor', 'major'].forEach(function(release) {
    grunt.registerTask(release, [`version:a:${release}`, 'build', 'version:dist']);
  });
};
