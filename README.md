# Fusionary JavaScript

This repo contains a bunch of plain JavaScript functions and jQuery plugins that we use often at Fusionary. They are sparsely documented below.

## ES6 Modules

For any of the following modules, you can do this:

```js
import * as examples from 'fmjs/example'

examples.example1('foo');
examples.example2('bar');
```

or this:

```js
import {example1, example2} from 'fmjs/example'

example1('foo');
example2('bar');
```


### core.js
This module is different from the others in that it creates a global `FM` object if one doesn't already exist and adds `.extend()` and `.getProperty()` methods to it. The methods can also be imported just like functions in the other modules.

* `{extend}`. Arguments: (`mutatee`, `object1`, `objectN`)
* `{getProperty}`. Arguments: (`rootObject'`, `string.of.props or [array, of, props]`)

### addelement.js
A couple convenience functions for loading a script or stylesheet.

* `{addScript}`. Arguments: (url, scriptId, callback)
* `{addLink}`. Arguments: (params)
  * `params`: `{media: 'screen', rel: 'stylesheet', type: 'text/css', href: ''}`

### analytics.js
A function for loading the Google Analytics script

* `{analytics}`. Arguments: (googleAnalyticsId, type)
  * `type`: either `undefined` or `'legacy'`

### array.js
A few array functions for convenience.

* `{isArray}`. Arguments: (`array?`). Returns Boolean
* `{inArray}`. Arguments: (`item`, `array`). Returns Boolean
* `{pluck}`. Arguments: (`array`, `property`)

### event.js
An `addEventListener` wrapper that naïvely deals with oldIE inconsistency and handles window load similar to how jQuery handles document ready (by triggering handler immediately if called _after_ the event has already fired).

* `{addEvent}`. Arguments: (`element`, `eventType`, `callback`)

### jsonp.js
For those times when you just need to make a "jsonp" request (and you can't set up CORS on the server).

* `{getJSONP}`. Arguments: (`url`, `options`) or (`options`, `fn`)
  * `options`. `{complete: function() {}, data: '', url: ''}`

### math.js

A handful of simple math functions that take arrays.

* `{add}`. Arguments: (`arrayOfNumbers`, `optionalStart`)
* `{subtract}`. Arguments: (`arrayOfNumbers`, `optionalStart`)
* `{multiply}`. Arguments: (`arrayOfNumbers`, `optionalStart`)
* `{divide}`. Arguments: (`arrayOfNumbers`, `optionalStart`)
* `{mod}`. Arguments: (`[a, b]`) or (`a`, `b`)

### selection.js

Some functions for text selection.

* `{getSelection}`. Arguments: (`element`). Returns Object (`{start, end, length, text}`)
* `{setSelection}`. Arguments: (`element`, `startPosition`, `endPosition`)
* `{replaceSelection}`. Arguments: (`element`, `replacementString`)

### shuffle.js

A function to randomly shuffle an array, using the Fisher-Yates/Knuth method

* `{shuffle}`. Arguments: (`array`)

### string.js

A bunch of handy string functions

* `{pluralize}`. Arguments: (`string`, `number`, `ending`)
* `{slugify}`. Arguments: (`string`)
* `{commafy}`. Arguments: (`number`)
* `{formatNumber}`. Arguments: (`number`, `options`)
  * `options`. `{prefix, suffix, classRoot}`
* `{rot13}`. Arguments: (`string`)
* `{hashCode}`. Arguments: (`string`)
* `{base64Encode}`. Arguments: (`string`)
* `{base64Decode}`. Arguments: (`encodedString`)

### timer.js

A couple functions for slowing things down a bit.

* `{debounce}`. Arguments: (`function`, `delay`, `context`)
* `{throttle}`. Arguments: (`function`, `delay`, `context`)

### touchevents.js

This is dumb and untested and it doesn't work right. Will probably yank it.

### transition-event.js

EZ access to the correct property name (vendor-prefixed or not) for `transitionend` and `animationend`

* `{transitionEnd}`
* `{animationEnd}`

### url.js

* `{pathname}`. Arguments: (optional `elementWith.href or string`). If no argument is specified, uses `location`.
* `{basename}`. Arguments: (optional `elementWith.href` or `string`, `extension`).
* `{hashSanitize}`. Arguments: (`hashString`). Removes potentially harmful characters and escapes dots.
* `{serialize}`. Arguments: (`object`, `options`)
  * `options`: `{raw: false, prefix: undefined, indexed: undefined}`
    * If `prefix` is set and `indexed` is not explicitly `false`, will treat arrays as `prefix[0]=zero&prefix[1]=want`
* `{unserialize}`. Arguments: (`queryString`, `options`). If no queryString provided, uses `location.search`
  * `options`: `{raw: false, empty: true}`. The `empty` option provides the value of a param with no value (e.g. by default, `?foo&bar=baz` would unserialize to `{foo: true, bar: 'baz'}`)

### youtube.js
A function to extract the YouTube ID from an element's `href` attribute or a URL string.

* `{getYoutubeId}`. Arguments: (`elementOrUrl`)

## jQuery Plugins

All of the following require jQuery. Some require other things, as noted.

### jquery.accordiontabs.js

`$.fn.accordiontabs(options)`

Extends the tabs widget so that tabs will turn into accordion at a certain breakpoint.

**options**:

Same options as `.tabs()`, plus:

* `breakpoint`: Number. Default `undefined`
* `isAccordion`: Function
* `accordionCollapsible`: Boolean
* `isAccordionClass`: String
* `accordionShow`: Object `{effect: 'slideDown', duration: 200}`
* `headingHtml`: Function



**Requires**:
* jQuery UI Tabs
* Custom CSS to change the display of tabs nav, etc. depending on presence of isAccordionClass

### jquery.cachedajax.js

`$.cachedAjax(url, options)`

Same options  as `$.ajax()`

Returns the `jqXHR` Promise-like object

**Requires**:
* Begins with `import {serialize} from './url'`, so you don't have to do anything if you're using it with the rest of the repo and using Babel/Webpack/whatever

### jquery.modal.js

Modal plugin.

**Usage**

```js
// Initialize
$('.foo').modal({
  beforeOpen: function(data) {
    data.content.empty().append('<div>hello world!</div>');
  }
});

// Open
$('.foo').on('click', function() {
  $('.foo').modal('open', otherOptions);
});

```
**Styles**

Default stylesheet is at css/jquery.modal.css. Note that if you change templates, the default styles may no longer work.

**Default options**:

```js
settings = {
  keepFocus: true,
  templates: {
    modal: '<div class="Modal" aria-live="polite"></div>',
    content: '<div class="Modal-content"></div>',
    closeDiv: '<div class="Modal-close"></div>',
    closeBtn: '<button class="Modal-closeBtn Button Button--close">&times;</button>'
  },
  classes: {
    modal: 'js-modal',
    content: 'js-modalContent',
    closeDiv: 'js-closeModalWrapper',
    closeBtn: 'js-closeModal',
    active: 'is-active'
  },

  // events
  onInit: null,
  beforeOpen: null,
  // If show === false in the callback arg, it does not go through with the open:
  beforeOpenAsync: null, // function(data, callback) { /* do something async */ callback(show); }
  afterOpen: null,
  beforeClose: null,
  afterClose: null
};
```

**Methods**
* `init`: This is the default method. Creates the modal div and appends it to the body.
* `open`: "Opens" the modal by adding the "is-active" class.
* `close`: "Closes" the modal by removing the "is-active" class.
* `destroy`: Removes the modal and its data & events.
* `options`: Gets or sets options for the modal.


### jquery.formfield.js

`$.fn.formField(options)`

Widget-like plugin that allows for the "float-label" thing.

**Default options**:

```js
settings = {
  emptyClass: 'is-empty',
  notEmptyClass: 'is-notEmpty',
  focusClass: 'is-focused',
  requiredClass: 'is-required',
  containers: '.FormField--floatLabel',
  inputs: '.FormField-input',
  labels: '.FormField-label'
};
```

**Methods**:
* `init`: This is the default method.
* `setFields`: e.g. `$('form or formFieldContainer(s)').formField('setFields', options)`. This is helpful if `.formField()` has already been called on the form and fields are injected later .
* `destroy`: e.g. `$('form or formFieldContainer(s)').formField('destroy')`
* `options`

**Requires**:
* Custom CSS. A base css file can be imported from fmjs/css/jquery.formfield.css

### jquery.imagesloaded.js

`$.fn.imagesLoaded(function, context)`

Executes a function when images have loaded.
* If none of the selected elements is an image, executes once for each selected element once all images within it have loaded.
* If any of the selected elements is an image itself, executes when all those images have loaded

Default context is first image if there are images in the collection, or current DOM element if no images in collection.

### jquery.inlinesvg.js

### jquery.social.js

### jquery.textsizer.js
