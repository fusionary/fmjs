import {extend} from './core';

// Insert a <script> element asynchronously
export let addScript = function addScript(url, sid, callback) {
  var loadScript = document.createElement('script');
  var script0 = document.getElementsByTagName('script')[0];
  var done = false;

  loadScript.async = 'async';
  loadScript.src = url;

  // In case someone puts the callback in the 2nd arg.
  if (typeof sid === 'function') {
    callback = sid;
    sid = null;
  }
  sid = sid || `s-${new Date().getTime()}`;

  // If there's a callback, set handler to call it after the script loads
  // NOTE: script may not be parsed by the time callback is called.
  if (callback) {
    loadScript.onload = loadScript.onreadystatechange = function() {
      if (!done &&
          (!this.readyState || this.readyState === 'loaded' || this.readyState === 'complete')
      ) {
        done = true;
        callback();
        loadScript.onload = loadScript.onreadystatechange = null;
        script0.parentNode.removeChild(loadScript);
      }

    };
  }

  if (!document.getElementById(sid)) {
    script0.parentNode.insertBefore(loadScript, script0);
  }
};

// Insert a CSS <link> element in the head.
export let addLink = function addLink(params) {
  var h = document.getElementsByTagName('head')[0];
  var opts = extend({
    media: 'screen',
    rel: 'stylesheet',
    type: 'text/css',
    href: ''
  }, params);

  // bail out if the <link> element is already there
  for (var i = 0, lnks = h.getElementsByTagName('link'), ll = lnks.length; i < ll; i++) {
    if (!opts.href || lnks[i].href.indexOf(opts.href) !== -1) {
      return;
    }
  }
  var lnk = document.createElement('link');

  for (var prop in opts) {
    lnk[prop] = opts[prop];
  }
  h.appendChild(lnk);

  lnk = null;

};
