/**
 * ajax
 * @type {function}
 * @param {url} String
 * @param {options} Object
 * @property {options.dataType} String One of 'json', 'html', 'xml', 'form', 'formData'
 * @property {options.data} Object|String
 * @property {options.method} String One of 'GET', 'POST', etc.
 * @property {options.cache} Boolean If set to false, will not let server use cached response
 * @property {options.memcache} Boolean If set to true, and previous request sent to same url was success, will circumvent request and use previous response
 * @property {options.headers} Object
 *
 */

import {serialize, unserialize} from './url.js';
import {getFormData} from './form.js';

let caches = {};

let appendQs = (url, data) => {
  let urlParts = url.split('?');
  let glue = urlParts.length === 2 ? '&' : '?';

  if (!data) {
    return url;
  }

  data = typeof data === 'string' ? data : serialize(data);

  return data ? `${url}${glue}${data}` : url;
};

let setHeaders = (xhr, headers = {}) => {
  for (let h in headers) {
    xhr.setRequestHeader(h, headers[h]);
  }
};

let setNoCache = (xhr, options) => {
  let {cache, headers} = options;
  let addHeaders = {};
  let testHeaders = {
    Pragma: 'no-cache',
    'Cache-Control': 'no-cache',
    'If-Modified-Since': 'Sat, 1 Jan 2000 00:00:00 GMT'
  };

  if (cache !== false) {
    return;
  }

  for (let h in testHeaders) {
    if (!headers[h]) {
      addHeaders[h] = testHeaders[h];
    }
  }

  setHeaders(xhr, addHeaders);
};

let setContentType = (xhr, dataType) => {
  let types = {
    json: {'Content-Type': 'application/json'},
    form: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
    formData: {'Content-Type': 'multipart/form-data'},
  };

  if (!dataType || !types[dataType]) {
    return;
  }

  setHeaders(xhr, types[dataType]);
};

let getResponseHeaders = function getResponseHeaders(xhr) {
  var allHeaders = xhr.getAllResponseHeaders().split('\r\n');
  var headers = {};

  allHeaders.forEach((item) => {
    // '\u003a\u0020' is ': '
    let headerParts = item.split('\u003a\u0020');

    if (headerParts < 2) {
      return;
    }

    let key = headerParts.shift();

    headers[key] = headerParts.join(' ');
  });

  return headers;
};

let processResponse = (xhr, opts) => {
  if (opts.dataType === 'json' && typeof xhr.response === 'string') {
    xhr.response = JSON.parse(xhr.response);
  }

  let response = {
    xhr: xhr,
    headers: getResponseHeaders(xhr),
    timestamp: +new Date(),
  };

  let responseKeys = [
    'response',
    'responseText',
    'responseType',
    'responseURL',
    'responseXML',
    'status',
    'statusText',
    'timeout'
  ];

  responseKeys.forEach((item) => {
    try {
      response[item] = xhr[item];
    } catch (e) {
      response[item] = null;
    }

  });

  return response;
};

let removeListeners = (xhr, events, handlers) => {
  events.forEach((event) => {
    xhr.removeEventListener(event, handlers[event]);
  });
};

export let ajax = function(url = location.href, options = {}) {
  let opts = Object.assign({
    method: 'GET',
    dataType: '',
    headers: {},
  }, options);
  let events = ['load', 'error', 'abort'];
  let isGet = /GET/i.test(opts.method);

  url = isGet ? appendQs(url, opts.data) : url;

  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();

    let handlers = {
      error: function(err) {
        removeListeners(xhr, events, handlers);
        reject(err);
      },
      abort: function() {
        removeListeners(xhr, events, handlers);
        reject(new Error({cancelled: true, reason: 'aborted', xhr}));
      },
      load: function() {
        removeListeners(xhr, events, handlers);

        if (xhr.status >= 400 || xhr.status < 200) {
          return reject(xhr);
        }

        let processedResponse = processResponse(xhr, opts);

        if (opts.memcache) {
          caches[url] = processedResponse;
        }

        return resolve(processedResponse);
      }
    };

    if (opts.memcache && caches[url]) {
      return resolve(caches[url]);
    }

    events.forEach((event) => {
      xhr.addEventListener(event, handlers[event]);
    });

    xhr.responseType = opts.dataType;
    xhr.open(opts.method.toUpperCase(), url);

    // Must set headers after xhr.open();
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    setHeaders(xhr, opts.headers);
    setNoCache(xhr, opts);

    if (isGet) {
      if (opts.dataType === 'json') {
        xhr.setRequestHeader('Accept', 'application/json');
      }

      xhr.send();
    } else {
      let dataType = opts.form ? '' : opts.dataType || 'form';

      setContentType(xhr, dataType);
      xhr.send(opts.data);
    }

  });
};

ajax.getJson = (url, options = {}) => {
  let opts = Object.assign({
    dataType: 'json',
    method: 'GET',
  }, options);

  return ajax(url, opts);
};

ajax.postJson = (url, options = {}) => {
  let opts = Object.assign({
    dataType: 'json',
    method: 'POST',
  }, options);

  if (typeof opts.data === 'object') {
    opts.data = JSON.stringify(opts.data);
  }

  return ajax(url, opts);
};

ajax.postFormData = (url, options = {}) => {
  let opts = Object.assign({
    data: new FormData(options.form),
    method: 'POST',
    dataType: '',
  }, options);

  return ajax(url, opts);
};
