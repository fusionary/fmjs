// Determine whether "arr" is a true array
export let isArray = function(arr) {
  return typeof arr === 'object' && Object.prototype.toString.call(arr) === '[object Array]';
};

// Determine whether item "el" is in array "arr"
export let inArray = function(el, arr) {
  if (arr.indexOf) {
    return arr.indexOf(el) !== -1;
  }

  for (var i = arr.length - 1; i >= 0; i--) {
    if (arr[i] === el) {
      return true;
    }
  }

  return false;
};

// Return a random item from the provided array
export let randomItem = function randomItem(arr) {
  let index = Math.floor(Math.random() * arr.length);

  return arr[index];
};

// Take an array of objects and a property and return an array of values of that property
export let pluck = function pluck(arr, prop) {
  arr = arr || [];

  return arr.map(function(el) {
    return el[prop] || false;
  });
};
