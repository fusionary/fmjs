// Core FM constructor. Load this first
'use strict';

// Deep merge two or more objects in turn, with right overriding left
// Heavily influenced by/mostly ripped off from jQuery.extend
let core = {
  extend: function extend(target) {
    target = Object(target);
    var arg, prop, targetProp, copyProp;
    var hasOwn = Object.prototype.hasOwnProperty;
    var isArray = Array.isArray || function(obj) {
      return Object.prototype.toString.call(obj) === '[object Array]';
    };
    var isObject = function isObject(obj) {
      return typeof obj === 'object' && obj !== null && !obj.nodeType && obj !== window;
    };

    // Set i = 1 to start merging with the 2nd argument into target;
    var i = 1;

    // If there is only one argument, merge it into `this` (starting with  1st argument)
    if (arguments.length === 1) {
      target = this;
      i--;
    }

    // No need to define i (already done above), so use empty statement
    for (; i < arguments.length; i++) {
      arg = arguments[i];

      if (isObject(arg)) {
        for (prop in arg) {
          targetProp = target[prop];
          copyProp = arg[prop];

          if (targetProp === copyProp) {
            continue;
          }

          if (isObject(copyProp) && hasOwn.call(arg, prop)) {
            if (isArray(copyProp)) {
              targetProp = isArray(targetProp) ? targetProp : [];
            } else {
              targetProp = isObject(targetProp) ? targetProp : {};
            }

            target[prop] = extend(targetProp, copyProp);
          } else if (typeof copyProp !== 'undefined') {
            target[prop] = copyProp;
          }
        }
      }
    }

    return target;
  },

  // Get value of a nested property, without worrying about reference error
  getProperty: function getProperty(root, properties) {
    root = root || window;
    properties = typeof properties === 'string' ? properties.split(/\./) : properties || [];

    return properties.reduce((acc, val) => {
      return acc && acc[val] ? acc[val] : null;
    }, root);
  },

  // Number of pixels difference between touchstart and touchend necessary
  // for a swipe gesture from fm.touchevents.js to be registered
  touchThreshold: 50
};

// Export each core method separately
export let extend = core.extend;
export let getProperty = core.getProperty;
export let touchThreshold = core.touchThreshold;

// Add FM with its core methods to global window object if window is available
if (typeof window !== 'undefined') {

  if (typeof window.FM === 'undefined') {
    window.FM = {};
  }

  for (let name in core) {
    let prop = core[name];

    if (typeof window.FM[name] === 'undefined') {
      window.FM[name] = prop;
    }
  }
}
