/** =generic addEventListener
    makes for more reliable window.onload.
************************************************************/
var listener = {
  prefix: ''
};

window.FM = window.FM || {};

window.FM.windowLoaded = document.readyState === 'complete';

if (typeof window.addEventListener === 'function') {
  listener.type = 'addEventListener';
} else if (typeof document.attachEvent == 'function' || typeof document.attachEvent == 'object') {
  listener.type = 'attachEvent';
  listener.prefix = 'on';
} else {
  listener.prefix = 'on';
}

export let addEvent = listener.type ? function(el, type, fn) {
  // Call immediately if window already loaded and calling addEvent(window, 'load', fn)
  if (window.FM.windowLoaded && type === 'load' && el === window) {
    fn.call(window, {windowLoaded: true, type: 'load', target: window});
  } else {
    el[ listener.type ](listener.prefix + type, fn, false);
  }

} : function(el, type, fn) {
  el[listener.prefix + type] = fn;
};

// call addEvent on window load
if (!window.FM.windowLoaded) {
  addEvent(window, 'load', function() {
    window.FM.windowLoaded = true;
  });
}
