import * as FM from './core';

// import './selection.js';
import './touchevents.js';

import * as addElements from './addelement.js';
import * as ajaxes from './ajax.js';
import * as ga from './analytics.js';
import * as arrays from './array.js';
import * as events from './event.js';
import * as forms from './form.js';
import * as jsonp from './jsonp.js';
import * as maths from './math.js';
import * as selections from './selection.js';
import * as shuffles from './shuffle.js';
import * as storages from './storage.js';
import * as strings from './string.js';
import * as timers from './timer.js';
import * as transEvents from './transition-event.js';
import * as urls from './url.js';
import * as youtubes from './youtube.js';

FM.extend(FM, addElements, ajaxes, ga, arrays, events, forms, jsonp, maths, selections);
FM.extend(FM, shuffles, storages, strings, timers, transEvents, urls, youtubes);

FM.extend(window.FM || {}, FM);
