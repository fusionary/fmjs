
const selectVal = function(select) {
  let val = select.type === 'select-one' ? null : [];
  let index = select.selectedIndex;
  let options = select.options;
  let selected = options[index];

  if (selected.disabled || index < 0) {
    return val;
  }

  val = selected.value;

  if (val == null) {
    val = selected.text;
  }

  return val ? val.replace(/\s\s+/g, ' ').trim() : val;
};

export const getFormData = (form) => {
  let elems = form.querySelectorAll('input, textarea, select');

  let formData = new FormData();

  elems.forEach((elem) => {
    if (elem.disabled || (/radio|checkbox/.test(elem.type) && !elem.checked)) {
      return;
    }
    let val = elem.value;

    if (elem.nodeName === 'SELECT') {
      val = selectVal(elem);
    }

    formData.append(decodeURIComponent(elem.name), decodeURIComponent(elem.value));

    if (elem.type === 'file') {
      formData.append(elem.name, elem.files[0]);
    }
  });

  return formData;
};
