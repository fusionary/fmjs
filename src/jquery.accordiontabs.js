/* eslint-disable prefer-template */
/*!
 * ---------------------------------------------------------------------------
 * Accordion Tabs - v0.1.0 - 2013-10-29
 * Licensed MIT http://kswedberg.mit-license.org/
 * ---------------------------------------------------------------------------
 */

/**
 * @usage $('foo').accordionTabs(options);
 *
 * @requires jQuery, jQuery UI (Core, Widget, Tabs)
 * @options
 ** (All jQuery UI Tabs options )
 ** breakpoint | Number | Default: undefined
 ** isAccordion | Function | Default: see isAccordion function below.
 **               To retrieve: $(selector).accordionTabs('isAccordion')
 ** accordionCollapsible | Boolean | Default: true
 ** isAccordionClass | String | Default: 'ui-state-accordion'
 ** accordionShow: | Object | Default: { effect: 'slideDown', duration: 200 }
 ** headingHtml | Function | Default: function() {
      return $(this).wrap('<h2 class="ui-tabs-accordion-heading"></h2>').parent();
    }
 * -----------------------------------------------------------------------------
 *
 * CSS Considerations
 *
 * You will need to apply your own CSS to change the display of tabs nav, etc. depending on
 * presence of isAccordionClass.
 *
 * If using the default isAccordion function, make sure you are NOT applying display: none to
 * tabs nav. Use something like {position:absolute; left: -1000em; top: -1000em; } instead.
 * -----------------------------------------------------------------------------
 */
import jQuery from 'jquery';

(function($) {
var pluginName = 'accordionTabs';

var rAccordion = /accordion([A-Z])/;

var timers = {};
var $win = $(window);

var accordionReplacer = function(match, upper) {
  return upper.toLowerCase();
};

$.widget('fm.' + pluginName, $.ui.tabs, {
  options: {
    accordionCollapsible: true,
    accordionShow: {
      effect: 'slideDown',
      duration: 200
    },
    isAccordionClass: 'ui-state-accordion',
    headingHtml: function() {
      return $(this).wrap('<h2 class="ui-tabs-accordion-heading"></h2>').parent();
    }
  },
  isAccordion: function() {
    if (typeof this.options.isAccordion === 'function') {
      return this.options.isAccordion.call(this);
    }
    var accord;
    var width = 0;
    var winWidth = $win.width();
    var breakpoint = this.options.breakpoint;

    this.tablist.width(this.element.width());

    this.tabs.each(function() {
      width += $(this).outerWidth();
    });

      // It's an accordion if window width < breakpoint
      // or sum of item widths is greater than ul/ol width
    accord = breakpoint && winWidth < breakpoint || this.element.width() < width;

    this.tablist.css({width: ''});

    return accord;
  },
  onresize: function(uuid) {
    clearTimeout(timers[uuid]);
    var self = this;

    timers[uuid] = setTimeout(function() {
      var isAccordion = self.isAccordion();

        // Map options to accordion/tabs options depending on if isAccordion
      $.each(self.options, function(key, val) {
        var root;

        if (key.indexOf('accordion') !== -1) {
          root = key.replace(rAccordion, accordionReplacer);
          self.options[root] = isAccordion ? val : self.options['_' + root];
        }
      });

        // Show first panel if none active and if collapsible option set to false
      if (!self.options.collapsible && !self.active.length) {
        self.anchors.first().trigger(self.options.event);
      }

        // Toggle the 'is-accordion' class
      self.element.toggleClass(self.options.isAccordionClass, isAccordion);
    }, 150);
  },
  _create: function() {
    this._super();
    this._uuid = new Date().getTime();

    var self = this;
    var $clones = this.anchors.clone();
    var activeIndex = -1;

    $.each(this.options, function(key, val) {
      if (key.indexOf('accordion') !== -1) {
        key = key.replace(rAccordion, accordionReplacer);
        self.options['_' + key] = self.options[key];
      }
    });

    if (self.options.active !== false) {
      activeIndex = self.options.active;
    }

    this.panels.each(function(index) {
      var $heading;
      var $clone = $clones.eq(index);

      $clone.prop({
        className: 'ui-accordion-anchor',
        id: function(i, val) {
          return 'acc-' + val;
        }
      });

      $clone.on(self.options.event + '.' + pluginName, function(event) {
        event.preventDefault();
        self.anchors.eq(index).trigger(self.options.event);

      });

      $heading = self.options.headingHtml.call($clone[0]);

      if (index === activeIndex) {
        $heading.addClass('is-active');
      }

      $(this).before($heading);
    });

      // Execute this right away to set proper className on tabs container
    this.onresize.call(self, this._uuid);

      // And execute after resize (onresize is debounced)
    $win.on('resize.' + pluginName, function() {
      self.onresize.call(self, self._uuid); // eslint-disable-line no-underscore-dangle
    });

    return this;
  },
  _toggle: function(event, eventData) {
    eventData.newPanel.prev().addClass('is-active');
    eventData.oldPanel.prev().removeClass('is-active');

    return this._superApply(arguments);
  },
  _setOption: function(key, value) {
    this._super(key, value);

    if (key.indexOf('accordion') === -1) {
      this.options['_' + key] = value;
    }

  },
  _destroy: function() {
    this._uuid = null;
    this.panels.prev().remove();
    $win.off('resize.' + pluginName);
    this._super();
  }
});
})(jQuery);
