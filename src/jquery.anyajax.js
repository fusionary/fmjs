/* eslint-disable prefer-template */
import jQuery from 'jquery';
(function($) {
$.anyAjax = function(url, options) {
  url = encodeURIComponent(url);

  var allOrigins = 'https://allorigins.pw/get?url=';
  var opts = $.extend({}, options, {
    dataType: 'json'
  });

  return $.ajax(allOrigins + url + '&callback=?', opts);
};
})(jQuery);
