/* eslint-disable prefer-template */
/** =cachedAjax plugin
* Requires serialize() from url
************************************************************/
import {serialize} from './url';
import jQuery from 'jquery';
(function($) {
$.cachedAjax = function(url, opts) {
  if ($.isPlainObject(url)) {
    opts = url;
    url = opts.url || location.pathname;
  }
  opts = opts || {};

  var nsCache = $.cachedAjax.caches[ url ] || {};
  var thisRequest = 'request';

    // if the request has data, we need to set cache key to url[data]
  if (opts.data) {
    thisRequest = $.isPlainObject(opts.data) ? serialize(opts.data) : opts.data;
  }

  if (!nsCache[ thisRequest ]) {
    nsCache[ thisRequest ] = $.ajax(url, opts);
  }

    // reset the global cache
  $.cachedAjax.caches[ url ] = nsCache;

  return nsCache[ thisRequest ];
};

$.cachedAjax.caches = {};
})(jQuery);
