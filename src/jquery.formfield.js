/* eslint-disable prefer-template */
// jQuery formField Plugin
import jQuery from 'jquery';
(function($) {
var settings = {
  emptyClass: 'is-empty',
  notEmptyClass: 'is-notEmpty',
  focusClass: 'is-focused',
  requiredClass: 'is-required',
  containers: '.FormField--floatLabel',
  inputs: '.FormField-input',
  labels: '.FormField-label'
};

var rFocusIn = /^focus(?:in)?$/;

var toggle = {
  emptyClass: function(el, opts) {
    var $container = $(el);
    var hasVal = opts.forceFilled || !!$container.find(opts.inputs).val();

    opts.forceFilled = false;

    $container
      .toggleClass(opts.emptyClass, !hasVal)
      .toggleClass(opts.notEmptyClass, hasVal);
  },
  focusClass: function(el, event, opts) {
    $(el).toggleClass(opts.focusClass, rFocusIn.test(event.type));
  },
  placeholderProp: function(el, eventType, opts) {
    var $container = $(el);
    var dataProp = rFocusIn.test(event.type) ? 'placeholder' : 'label';
    var placeholderText = $container.data(dataProp);

    $container.find(opts.inputs).prop({placeholder: placeholderText});
  }
};

var getData = function(el) {
  var $container = $(el);
  var data = $container.data('formfield') || {};

  return data;
};

var setRequired = function($container, opts) {
  var $input = $container.find(opts.inputs);

    // Need to check "required" class, too, in case tinyvalidate has already changed attr to class.
  if ($input.is('[required]') || $input.hasClass('required')) {
    $container.addClass(opts.requiredClass);
  }
};
var setInitialText = function($container, opts) {
  var dataLabel = $container.data('label');
  var dataPlaceholder = $container.data('placeholder');
  var $input = $container.find(opts.inputs);

  if (!dataLabel) {
    dataLabel = $container.find(opts.labels).text();
    $container.data('label', dataLabel);
  }

  if (!dataPlaceholder) {
    dataPlaceholder = $input.prop('placeholder');
    $container.data('placeholder', dataPlaceholder);
  }

  $input.prop('placeholder', dataLabel);
};

var setup = function() {
  var $container = $(this);
  var opts = getData(this).options || {};

  toggle.emptyClass(this, opts);
  setRequired($container, opts);
  setInitialText($container, opts);

  $container
    .off('.formfield')
    .on('focusin.formfield focusout.formfield', function(event) {
      toggle.focusClass(this, event, opts);
      toggle.placeholderProp(this, event, opts);
    })
    .on('focusout.formfield change.formfield keyup.formfield', function() {
      toggle.emptyClass(this, opts);
    });
};

var methods = {

  init: function(options) {
    options = $.extend(true, {}, $.fn.formField.defaults, options || {});
    this.each(function() {
      var $this = $(this);

      if ($this.data('formfield')) {
        return;
      }

      methods.options.call($this, options);

      if ($this.is(options.containers)) {
        $this.each(setup);
      } else {
        $this.find(options.containers).each(setup);
      }
    });

    return this;
  },
  setFields: function(options) {
    this.each(function() {
      var $this = $(this);

      options = $.extend(getData(this).options, options || {});
      methods.options.call($this, options);

      if ($this.is(options.containers)) {
        $this.each(setup);
      } else {
        $this.find(options.containers).each(setup);
      }
    });

    return this;
  },
  destroy: function() {
    var cleanup = function() {

      var $container = $(this);

        // Remove created elements, off namespaced events, and remove data
      $container.off('.formfield')
        .removeData('formfield');
    };

    this.each(function() {
      var $this = $(this);
      var data = $this.data('formfield');

      if (data) {
        $this.each(cleanup);
        $this.find(data.options.containers).each(cleanup);
      }

      $this = data = null;
    });

    return this;
  },
  options: function(options) {

    if (!options) {
      options = getData(this[0]) || {};

      return options || {};
    }

    options = $.extend(true, {}, $.fn.formField.defaults, options);

    var setOptions = function() {
      var $container = $(this);
        // don't use our getData() function here
        // because we want an object regardless
      var data = $container.data('formfield') || {
        formfield: true
      };
      var opts = data.options || {};

        // deep extend (merge) default settings, per-call options, and options set with:
        // html5 data-formfield options JSON and $('selector').formField( 'options', {} );
      opts = $.extend(true, {}, opts, options);
      data.options = opts;
      $(this).data('formfield', data);

    };

    this.each(setOptions);

    if (!this.is(options.containers)) {
      this.find(options.containers).each(setOptions);
    }

    return this;
  }
};

var protoSlice = Array.prototype.slice;

$.fn.formField = function(method) {

  if (methods[method]) {
    return methods[method].apply(this, protoSlice.call(arguments, 1));
  } else if (typeof method === 'object' || !method) {
    return methods.init.apply(this, arguments);
  } else {
    $.error('Method ' +  method + ' does not exist on jQuery.fn.formField');
  }

};

$.extend($.fn.formField, {
  defaults: settings
});

})(jQuery);
