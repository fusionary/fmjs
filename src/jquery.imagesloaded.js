/* eslint-disable prefer-template */
/*!
 * jQuery Images Loaded Plugin
 *
 * @version 1.0.0
 * @date 2014-09-16
 * @copyright (c) 2014 Karl Swedberg
 * @license MIT http://kswedberg.mit-license.org/
 *
 */

  /**
   * @description Executes a function when images have loaded.
   * If none of the selected elements is an image, executes once for each selected element once all
   *   images within it have loaded.
   * If any of the selected elements is an image itself, executes when all those images have loaded
   *
   * @requires jQuery
   * @param {function} fn - The function you want to call once all the images have loaded
   * @param {object} ctx - Optional. The context (for setting `this`). Defaults to first img if imgs
   *   in collection or current DOM element if no imgs in collection
   *
   * @example $('.container').imagesLoaded(fn, ctx);
   * @description Executes fn once for each .container after all images within .container have loaded
   *
   * @example $('img.my-img').imagesLoaded(fn, ctx);
   * @description Executes fn once after all images class="my-img" have loaded. ctx default is first img in collection
   *
   */
import jQuery from 'jquery';
(function($) {
$.fn.imagesLoaded = function(fn, ctx) {
  var $images = this.filter(function() {
    return this.nodeName === 'IMG';
  });

  var checkLoaded = function checkLoaded($img) {
    ctx = ctx || ($img && $img[0]) || this;
    $img = $img || $(this).find('img');

    var loaded = 0;
    var imgLen = $img.length;
    var execFn = $.proxy(fn, ctx);

    if (!imgLen) {
      execFn();

      return;
    }

    var incrementAndRun = function incrementAndRun() {
      loaded++;

      if (loaded >= imgLen) {
        execFn();
        $img.off('.loadImages');
      }
    };

    $img.each(function() {
      if (this.complete) {
        incrementAndRun();
      } else {
        $(this).on('load.loadImages error.loadImages', incrementAndRun);
      }
    });
  };

  if ($images.length) {
    checkLoaded($images);
  } else {
    this.each(function() {
      // Assume that we're dealing with a container of imgs, so call checkLoaded with no args:
      checkLoaded.call(this);
    });
  }

  return this;
};
})(jQuery);
