/* eslint-disable prefer-template */
/*!
 * jQuery inlineSvg Plugin - v1.0.0 - 2015-05-05
 *
 * Copyright (c) Karl Swedberg
 * Licensed MIT http://kswedberg.mit-license.org/
 *
 * REQUIRES jQuery (http://jquery.com/)
 */
import jQuery from 'jquery';
(function($) {

$.fn.inlineSvg = function(settings) {

  if (!this.length) {
    return this;
  }

  var options = $.extend(true, {}, $.fn.inlineSvg.defaults, settings);

  this.each(function() {
    var $this = $(this);
    var opts = $.extend(true, {}, options, $this.data());
    var path = [opts.spriteFile, opts.spriteId].join('#');
    var svgAttrs = '';

    $.each(opts.svgAttrs, function(attr, val) {
      svgAttrs += ' ' + attr + '="' + val + '"';
    });
    var svg = '<svg' + svgAttrs + '>';

    svg += '<use xlink:href="'  + path + '"></use></svg>';
    $this[opts.method](svg);
  });

  return this;
};

  // default options
$.fn.inlineSvg.defaults = {
  method: 'append',
  spriteId: null,
  spriteFile: '/assets/img/sprites/sprites.svg',
  svgAttrs: {},
};

})(jQuery);
