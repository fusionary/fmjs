/* eslint-disable prefer-template */
// cf http://mobile.smashingmagazine.com/2013/09/16/responsive-images-performance-problem-case-study/

// HTML:
// <noscript data-src-small="img-small.jpg"
//     data-src-medium="img-medium.jpg"
//     data-src-high="img-high"
//     data-src-x-high="img-x-high.jpg">
//         <img src="img-small.jpg">
// </noscript>
import jQuery from 'jquery';
(function($) {

var screenPixelRatio = function() {
  var retVal = 1;

  if (window.devicePixelRatio) {
    retVal = window.devicePixelRatio;
  } else if (window.matchMedia) {
    if (window.matchMedia('(min-resolution: 2dppx)').matches || window.matchMedia('(min-resolution: 192dpi)').matches) {
      retVal = 2;
    } else if (window.matchMedia('(min-resolution: 1.5dppx)').matches || window.matchMedia('(min-resolution: 144dpi)').matches) {
      retVal = 1.5;
    }
  }

  return retVal;
};

var getImageVersion = function() {

  var pixelRatio = screenPixelRatio();
  var width = window.innerWidth * pixelRatio;
    // default size:
  var size = 'small';
    // sizes: small = 320, medium = 640, high = 720

  if (width > 320 && width <= 640) {
    size = 'medium';
  } else if (width > 640 && pixelRatio > 1) {
    size = 'high';
  } else if (width > 640) {
    size = 'x-high';
  }

  return size;
};

var lazyloadImage = function(imageContainer, imageVersion) {

  if (!imageContainer || !imageContainer.children) {
    return;
  }
  var img = imageContainer.children[0];

  if (img) {
    var imgSRC = img.getAttribute('data-src-' + imageVersion);
    var altTxt = img.getAttribute('data-alt');

    if (imgSRC) {
      try {
        var imageElement = new Image();

        imageElement.src = imgSRC;
        imageElement.setAttribute('alt', altTxt ? altTxt : '');
        imageContainer.appendChild(imageElement);
      } catch (e) {
        console.log('img error' + e);
      }
      imageContainer.removeChild(imageContainer.children[0]);
    }
  }
};

$.fn.loadImages = function() {
  var imageVersion = getImageVersion();

  return this.each(function() {
    lazyloadImage(this, imageVersion);
  });
};

})(jQuery);
