/* eslint-disable prefer-template */
/*!
 * jQuery Social Plugin - v0.1.1 - 2014-01-29
 *
 * Copyright (c) 2014 Karl Swedberg
 * Licensed MIT http://kswedberg.mit-license.org/
 *
 * REQUIRES jQuery (http://jquery.com/)
 */

/*
 * Make sure the links have the first part of the social site's URL in the href:
 * facebook: http://www.facebook.com/sharer.php?
 * twitter: https://twitter.com/?
 * googleplus: https://plus.google.com/share?
 * linkedin: http://www.linkedin.com/shareArticle?
 * pinterest: http://pinterest.com/pin/create/button/?
 *
 * Easiest way to set up what you want the status message to have
 * is to use a data-social attribute on the link, with a JSON object as its value.
 *
 * Here are the data-social properties:
 ** site: one of "googleplus", "facebook", "twitter", "linkedin"
 ** title: The title for the social media status
 ** desc: The summary for the social media status
 ** img: Optional image to be used for the social media status
 **      If no img provided, will look for imgContainer option. If no imgContainer,
 **      looks for fallback
 ** id: optional id to be appended to the url, along with a hash ( #id ) for linking back to a
 **     specific part of the page
 *
 * Instead of the object, you can use these individual data-* attributes:
 ** data-site
 ** date-title
 ** data-desc
 ** data-img
 ** data-id
 *
 * PARAMS OBJECT:
 * The options object contains another object with the key "params"
 * The plugin uses this object to construct the query string by looping through
 * each key and calling its corresponding function.
 *
 * Other overall options (and their defaults):
 **
 ** url: location.href.replace(/(\?|#).*$/,''),
 ** imgContainer: A container element, relative to the clicked link, inside which to find an image
 ** fallbackImg: '',
 ** attrs: {
 **   target: '_blank'
 ** }
 */
import jQuery from 'jquery';
(function($) {

$.fn.social = function(options) {
  var settings = $.extend(true, {}, $.fn.social.defaults, options);

  this.each(function() {
    var queryString = '';
    var link = this;
    var $link = $(this);
    var linkAttrs = {};
    var params = [];
    var opts = $.extend(true, {}, settings, $link.data('social') || {});

      // Account for data-social object or individual data-* attributes:
    $.each(['site', 'title', 'desc', 'img', 'id'], function(index, opt) {
      opts[opt] = opts[opt] || $link.data(opt);
    });

    var social = {
      googleplus: {
        url: opts.params.url
      },
      facebook: {
        s: 100,
        'p[url]': opts.params.url,
        'p[images][0]': opts.params.img,
        'p[title]': opts.params.title,
        'p[summary]': opts.params.desc
      },
      twitter: {
        status: opts.params.status
      },
      linkedin: {
        mini: 'true',
        url: opts.params.url,
        title: opts.params.title,
        summary: opts.params.desc
      },
      pinterest: {
        url: opts.params.url,
        media: opts.params.img,
        description: opts.params.desc
      },
      email: {
        subject: opts.params.title,
        body: opts.params.body || ''
      }
    };

    if (social[opts.site]) {

      $.each(social[opts.site], function(key, val) {
        if (typeof val === 'function') {
          val = val(link, opts);
        }

        if (val) {
            // params.push( key + '=' + val );

          params.push(encodeURIComponent(key) + '=' + encodeURIComponent(val));
        }

      });

      queryString = params.join('&');
      linkAttrs = $.extend({}, opts.attrs, {
        href: function(i, val) {
          return val += queryString;
        }
      });

      if (opts.site === 'email' && linkAttrs.target) {
        delete linkAttrs.target;
      }

      $link.prop(linkAttrs);
    }
  });

  return this;
};

$.fn.social.defaults = {
  url: location.href.replace(/(\?|#).*$/, ''),
  fallbackImg: '',
  attrs: {
    target: '_blank'
  },
  params: {
    url: function(link, opts) {
      var url = opts.url;

      if (opts.id) {
        url += '#' + opts.id;
      }

      return url;
    },
    title: function(link, opts) {
      return opts.title;
    },
    status: function(link, opts) {
      var title = opts.desc;
      var url = typeof opts.params.url === 'function' ? opts.params.url(link, opts) : opts.params.url;

      if ((title + '').length > 106) {
        title = title.slice(0, 106) + '...';
      }

      return title + ' – ' + url;
    },
    desc: function(link, opts) {
      return opts.desc;
    },
    img: function(link, opts) {
      var img = opts.img;

      if (!img) {
        img = $(link)
          .closest(opts.imgContainer)
          .find('img')
          .first()
          .prop('src');
      }

      return img || opts.fallbackImg;
    }
  }
};

})(jQuery);
