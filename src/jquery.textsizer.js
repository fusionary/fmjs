/* eslint-disable prefer-template */
/*!
 * jQuery Text Sizer Plugin - v0.1.0 - 2014-04-16
 *
 * Copyright (c) 2014 Karl Swedberg
 * Licensed MIT http://kswedberg.mit-license.org/
 *
 * REQUIRES jQuery (http://jquery.com/)
 */
import jQuery from 'jquery';
(function() {
$.fn.textSizer = function(options) {
  var opts = $.extend({}, $.fn.textSizer.defaults, options || {});

  this.each(function(index) {
    var $this = $(this);
    var $chars = $this.children().clone();
    var chars = '';
    var padding = parseFloat($this.css('paddingLeft')) + parseFloat($this.css('paddingRight'));
    var maxWidth = $this.parent().width() - padding;
    var initialStyles = {
      position: 'absolute',
      top: '-100em',
      left: '-1000em',
      display: 'inline-block',
      fontSize: opts.minFontSize + 'px',
      lineHeight: 1,
      maxWidth: maxWidth,
      letterSpacing: $this.css('letterSpacing')
    };

    var changeSize =  function(size) {
      $chars.css({fontSize: (size + 2) + 'px'});

      setTimeout(function() {
        var height = $chars.height();

        if (!height) {
          return;
        }

        if (height > size + 2 || size >= opts.maxFontSize) {
          if (size > opts.maxFontSize) {
            size = opts.maxFontSize;
          }

          $this.css({fontSize: size + 'px'});
          $chars.remove();
          $chars = $this = chars = padding = null;
        } else {
          changeSize(size + 2);
        }
      }, 20);
    };

    $chars.children().remove();
    chars = $chars.text();
    chars = chars.replace(/\s+/g, '');

    $chars = $('<div></div>').text(chars).css(initialStyles);

    if (opts.className) {
      $chars.addClass(opts.className);
    }

    $chars.appendTo('body');

    changeSize(opts.minFontSize);

  });

  return this;
};

$.fn.textSizer.defaults = {
  minFontSize: 8,
  maxFontSize: 200,
  className: ''
};
})();
