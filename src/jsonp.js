// Simple JSONP method for x-site script grabbing

let getOpts = function getOpts(options, cb) {
  let opts = typeof options === 'string' ? {url: options} : options;

  opts.complete = typeof cb === 'function' ? cb : opts.complete || function() {};
  opts.data = opts.data || {};

  return opts;
};

let head = document.getElementsByTagName('head')[0];

window.jsonp = {};

export let getJSONP = function(options, cb) {
  let opts = getOpts(options, cb);
  let hasJSON = typeof window.JSON !== 'undefined';
  let src = opts.url + (opts.url.indexOf('?') > -1 ? '&' : '?');

  let newScript = document.createElement('script');
  let params = [];
  let callback = `j${new Date().getTime()}`;

  opts.data.callback = opts.data.callback || `jsonp.${callback}`;

  // This function will be called in the other server's response
  window.jsonp[callback] = function(json) {

    if (!hasJSON) {
      json = {error: 'Your browser is too old and unsafe for this.'};
    } else if (typeof json === 'object') {
      let s = JSON.stringify(json);

      json = JSON.parse(s);
    } else if (typeof json === 'string') {
      json = JSON.parse(json);
    }

    // Callback function defined in options.complete
    // called after json is parsed.
    opts.complete(json);

    window.jsonp[callback] = null;
  };

  for (let key in opts.data) {
    params.push(`${key}=${encodeURIComponent(opts.data[key])}`);
  }
  src += params.join('&');

  newScript.src = src;

  // if (this.currentScript) head.removeChild(currentScript);
  head.appendChild(newScript);
  newScript.src = null;
};

