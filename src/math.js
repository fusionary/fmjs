var fnType = [].reduce ? 'reduce' : 'loop';

var fns = {
  add: function(a, b) {
    return a + b;
  },
  subtract: function(a, b) {
    return a - b;
  },
  multiply: function(a, b) {
    return a * b;
  },
  divide: function(a, b) {
    return a / b;
  },
};

var compute = {
  reduce: function reduce(operation) {
    return function(nums, start) {
      start = start || 0;

      return nums.reduce(fns[operation], start);
    };
  },
  loop: function loop(operation) {
    return function(nums, start) {
      start = start || 0;

      for (var i = nums.length - 1; i >= 0; i--) {
        start = fns[operation](start, nums[i]);
      }

      return start;
    };
  }
};

export let add = compute[fnType]('add');
export let subtract = compute[fnType]('subtract');
export let multiply = compute[fnType]('multiply');
export let divide = compute[fnType]('divide');

export let mod = function mod(a, b) {
  // Allow for either two numbers or an array of two numbers
  if (arguments.length === 1 && typeof a === 'object' && a.length === 2) {
    b = a[1];
    a = a[0];
  }

  return a % b;
};
