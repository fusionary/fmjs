// Text selection functions

// Use these functions for oldIE
var normalizeCharLength = function(slicedChars, allChars) {
  var chr = '';
  var adjust = 0;

  for (var i = 0; i < slicedChars.length; i++) {
    chr = slicedChars[i];

    if (chr === '\r' && allChars[i + 1] === '\n') {
      adjust -= 1;
    }
  }

  return adjust;
};

var ieSetSelection = function ieSetSelection(elem, chars, startPos, endPos) {
  elem.focus();
  var textRange = elem.createTextRange();

  // Fix IE from counting the newline characters as two seperate characters
  var startChars = chars.slice(0, startPos);
  var endChars = chars.slice(startPos, endPos - startPos);
  var chr = '';
  var i = 0;

  for (i = 0; i < startChars.length; i++) {
    chr = startChars[i];

    if (chr === '\r' && chars[i + 1] === '\n') {
      startPos -= 1;
    }
  }
  startPos += normalizeCharLength(startChars, chars);
  endPos += normalizeCharLength(endChars, chars);

  textRange.moveEnd('textedit', -1);
  textRange.moveStart('character', startPos);
  textRange.moveEnd('character', endPos - startPos);
  textRange.select();
};

var ieGetSelection = function ieGetSelection(elem, val) {
  elem.focus();
  var range = document.selection.createRange();
  var textRange = elem.createTextRange();
  var textRangeDupe = textRange.duplicate();

  textRangeDupe.moveToBookmark(range.getBookmark());
  textRange.setEndPoint('EndToStart', textRangeDupe);

  // bail if nothing selected
  if (range == null || textRange == null) {
    return {
      start: val.length,
      end: val.length,
      length: 0,
      text: ''
    };
  }

  // For some reason IE doesn't always count the \n and \r in the length
  var textPart = range.text.replace(/[\r\n]/g, '.');
  var textWhole = val.replace(/[\r\n]/g, '.');
  var textStart = textWhole.indexOf(textPart, textRange.text.length);

  return {
    start: textStart,
    end: textStart + textPart.length,
    length: textPart.length,
    text: range.text
  };
};

// Set the selection of an element's contents.
// NOTE: If startPos and/or endPos are used on a non-input element,
// only the first text node within the element will be used for selection

export let setSelection = function setSelection(elem, startPos, endPos) {
  startPos = startPos || 0;

  var val = elem.value || elem.textContent || elem.innerText;
  var chars = val.split('');
  var selection, range, textNode;

  if (typeof endPos === 'undefined') {
    endPos = chars.length;
  }

  // only for inputs
  if (elem.nodeName === 'INPUT' && typeof elem.selectionStart !== 'undefined') {
    elem.focus();
    elem.selectionStart = startPos;
    elem.selectionEnd = endPos;

    return this;
  }

  if (window.getSelection) {
    selection = window.getSelection();
    range = document.createRange();

    if (startPos === 0 && endPos === chars.length) {
      range.selectNodeContents(elem);
    } else {
      textNode = elem;

      while (textNode && textNode.nodeType !== 3) {
        textNode = textNode.firstChild;
      }
      range.setStart(textNode, startPos);
      range.setEnd(textNode, endPos);
    }

    if (selection.rangeCount) {
      selection.removeAllRanges();
    }

    selection.addRange(range);

    return this;
  }

  // oldIE
  if (typeof elem.createTextRange !== 'undefined') {
    ieSetSelection(elem, chars, startPos, endPos);
  }

  return this;
};

export let getSelection = function getSelection(el) {
  var userSelection, length;
  var elem = el || document;
  var val = elem.value || elem.textContent || elem.innerText;

  // Modern browsers

  // Inputs
  if (elem.nodeName === 'INPUT' && typeof elem.selectionStart !== 'undefined') {
    length = elem.selectionEnd - elem.selectionStart;

    return {
      start: elem.selectionStart,
      end: elem.selectionEnd,
      length: elem.selectionEnd - elem.selectionStart,
      text: elem.value.slice(elem.selectionStart, length)
    };
  }

  // Other elements
  if (window.getSelection) {
    userSelection = window.getSelection();

    return {
      start: userSelection.anchorOffset,
      end: userSelection.focusOffset,
      length: userSelection.focusOffset - userSelection.anchorOffset,
      text: userSelection.toString()
    };
  }

  // oldIE
  if (document.selection) {
    return ieGetSelection(elem, val);
  }

  // Browser not supported
  return {
    start: val.length,
    end: val.length,
    length: 0,
    text: ''
  };

};

export let replaceSelection = function replaceSelection(elem, replaceString) {
  var selection = getSelection(elem);
  var startPos = selection.start;
  var endPos = startPos + replaceString.length;
  var props = ['value', 'textContent', 'innerText'];
  var prop = '';

  for (var i = 0; i < props.length; i++) {
    if (typeof elem[ props[i] ] !== 'undefined') {
      prop = props[i];
      break;
    }
  }

  elem[prop] = elem[prop].slice(0, startPos) + replaceString + elem[prop].slice(selection.end, elem[prop].length);

  setSelection(elem, startPos, endPos);

  return {
    start: startPos,
    end: endPos,
    length: replaceString.length,
    text: replaceString
  };
};

export let wrapSelection = function wrapSelection(elem, options) {
  var selectedText = getSelection(elem).text;
  var selection =  replaceSelection(elem, options.before + selectedText + options.after);

  if (options.offset !== undefined && options.length !== undefined) {
    selection = setSelection(elem, selection.start +  options.offset, selection.start +  options.offset + options.length);
  } else if (!selectedText) {
    selection = setSelection(elem, selection.start + options.before.length, selection.start + options.before.length);
  }

  return selection;
};
