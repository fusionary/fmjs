export let shuffle = function(els) {
  // Fisher-Yates (aka Knuth) shuffle
  // https://github.com/coolaj86/knuth-shuffle
  var temporaryValue, randomIndex;
  var currentIndex = els.length;

  // While there remain elements to shuffle...
  while (currentIndex !== 0) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = els[currentIndex];
    els[currentIndex] = els[randomIndex];
    els[randomIndex] = temporaryValue;
  }

  return els;
};
