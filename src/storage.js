import {extend} from './core';

let rNormalizeNs = /([^\w\$_-])+/g;
let nsReplace = function(ns, key) {
  ns = (ns || '').replace(rNormalizeNs, '');

  return key.replace(ns, '');
};

let Storage = function Storage(type, namespace) {
  if (!(this instanceof Storage)) {
    return new Storage(type);
  }

  if (type === 'session') {
    this.store = sessionStorage;
  } else {
    this.store = localStorage;
  }

  namespace = (namespace || '').replace(rNormalizeNs, '');
  this.namespace = namespace;
  this.length = this.getLength();

  return this;
};

Storage.prototype.getLength = function getLength() {
  if (!this.namespace) {
    return this.store.length;
  }

  return Object.keys(this.store).filter((item) => {
    return item.indexOf(this.namespace) === 0;
  }).length;
};

Storage.prototype.get = function get(key) {
  key = this.namespace + key;
  let data = this.store.getItem(key);

  return JSON.parse(data);
};

Storage.prototype.set = function set(key, value) {
  key = this.namespace + key;
  let data = JSON.stringify(value);

  this.store.setItem(key, data);
  this.length = this.getLength();

  return data;
};

Storage.prototype.remove = function remove(key) {
  key = this.namespace + key;
  this.store.removeItem(key);
  this.length = this.getLength();
};

Storage.prototype.clear = function clear() {
  if (!this.namespace) {
    this.store.clear();
  } else {
    this.keys().forEach((key) => {
      this.remove(key);
    });
  }

  this.length = 0;
};

Storage.prototype.getAll = function getAll() {
  let data = {};

  this.keys().forEach((key) => {
    data[key] = JSON.parse(this.store.getItem(this.namespace + key));
  });

  return data;
};

// Loop through all storage items and return an array of their keys
Storage.prototype.keys = function keys() {
  let data = [];

  for (let i = 0, len = this.store.length; i < len; i++) {
    let key = this.store.key(i);

    if (!this.namespace) {
      data.push(key);
    } else if (key.indexOf(this.namespace) === 0) {
      key = nsReplace(this.namespace, key);
      data.push(key);
    }
  }

  return data;
};

// Loop through all storage items, calling the callback for each one
Storage.prototype.each = function each(callback) {
  let len = this.store.length;
  let stores = this.getAll();

  for (let key in stores) {
    let ret = callback(key, stores[key]);

    if (ret === false) {
      return;
    }
  }
};

Storage.prototype.map = function map(callback) {
  let stores = this.getAll();
  let data = {};

  for (let key in stores) {
    let ret = callback(key, stores[key]);

    data[key] = ret;
  }

  return data;
};

Storage.prototype.mapToArray = function mapToArray(callback) {
  let stores = this.getAll();
  let data = [];

  for (let key in stores) {
    let ret = callback(key, stores[key]);

    data.push(ret);
  }

  return data;
};

//
Storage.prototype.filter = function filter(callback) {
  let data = {};
  let keys = this.keys();

  for (let i = 0, len = keys.length; i < len; i++) {
    let key = keys[i];
    let val = JSON.parse(this.store.getItem(this.namespace + key));

    if (callback(key, val, i)) {
      data[key] = val;
    }
  }

  return data;
};

// Assuming multiple storage items, with each one an object…
// Loop through all storage items and turn them into an array of objects
// with each object given a `key` property with value being the storage item's key
Storage.prototype.toArray = function toArray() {
  let data = [];

  return this.keys().map((key) => {
    let val = JSON.parse(this.store.getItem(this.namespace + key));

    val.key = key;

    return val;
  });
};

// Loop through all storage items, like .toArray()
// and filter them with a callback function with return value true/false
Storage.prototype.filterToArray = function filterToArray(callback) {
  let data = [];

  this.each(function(key, val) {
    let include = callback(key, val);

    if (include) {
      val.key = key;
      data.push(val);
    }
  });

  return data;
};

// Merge object into a stored object (referenced by 'key')
Storage.prototype.merge = function merge(deep, key, value) {
  let data = this.get(deep === true ? key : deep) || {};

  if (deep === true) {
    // deep extend
    data = extend(data, value);
  } else {
    // shallow, so need to rearrange args, then do a shallow copy of props
    value = key;
    key = deep;

    for (let k in value) {
      if (value.hasOwnProperty(k)) {
        data[k] = value[k];
      }
    }
  }

  this.set(key, data);

  return data;
};

export {Storage};
