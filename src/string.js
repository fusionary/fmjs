import {extend} from './core';
// Make word plural if num isn't 1
export let pluralize = function pluralize(str, num, ending) {
  num = num * 1;

  if (ending === undefined) {
    ending = 's';
  }

  if (num !== 1) {
    str += ending;
  }

  return str;
};

// Slugify a string by lowercasing it and replacing white spaces and non-alphanumerics with dashes.
export let slugify = function slugify(str) {
  var deDashed;

  str = (str || '').replace(/[^a-zA-Z0-9\-]+/g, '-').replace(/\'\-+/g, '-').toLowerCase();

  // remove leading and trailing dashes
  deDashed = str.replace(/^\-|\-$/g, '');

  return deDashed || str;
};

// Add commas to numbers
export let commafy = function commafy(val) {
  var tmp = '';
  var int = '';

  if (val > 999) {
    tmp = (`${val}`).split('');

    for (var i = tmp.length - 1, j = 0; i >= 0; i--) {
      int = tmp[i] + int;

      if (j++ % 3 === 2 && i) {
        int = `,${int}`;
      }
    }
    val = int;
  }

  return val;
};

export let decimalify = function decimalify(dec, places) {
  let i;

  dec = dec || '';

  if (dec.length === places) {
    return dec;
  }

  if (places == null) {
    return dec;
  }

  if (dec.length < places) {
    // If fewer decimal places than needed, "right-pad" with zeros
    for (i = dec.length; i < places; i++) {
      dec += '0';
    }
  } else if (dec.length > places) {
    dec = Math.round((dec * 1) / Math.pow(10, dec.length - places));
    dec = `${dec}`;

    // After rounding to the proper level, need to "left-pad" to match number of decimal places
    // For example, rounding to 2 decimal places, 0472323 will change to 05, which will be read only as 5
    for (i = dec.length; i < places; i++) {
      dec = `0${dec}`;
    }
  }

  return dec;
};

// Convert a number to a formatted string
export let formatNumber = function formatNumber(val, options) {
  options = options || {};
  var numParts = [];
  var num = '';
  var output = '';
  var opts = extend({
    prefix: '',
    suffix: '',
    // decimalPlaces: undefined
    includeHtml: typeof options.prefix === 'string' && options.prefix.indexOf('$') !== -1,
    classRoot: 'Price',
  }, options);

  var cr = opts.classRoot;

  num = `${val}`;
  numParts = num.split('.');

  numParts[1] = decimalify(numParts[1], opts.decimalPlaces);

  numParts[0] = commafy(numParts[0]);

  if (opts.includeHtml) {
    output = `<span class="${cr}"><span class="${cr}-prefix">${opts.prefix}</span>`;
    output += `<span class="${cr}-int">${numParts[0]}</span>`;

    if (opts.decimalPlaces !== 0) {
      output += `<span class="${cr}-dec">.</span>`;
      output += `<span class="${cr}-decNum">${numParts[1]}</span>`;
    }

    output += opts.suffix ? `<span class="${cr}-suffix">${opts.suffix}</span>` : '';
    output += '</span>';
  } else {

    output = opts.prefix + numParts.join('.') + opts.suffix;
  }

  return output;
};

// ROT13 encode/decode a string
export let rot13 = function rot13(str) {
  str = str.replace(/[a-zA-Z]/g, function(c) {
    var cplus = c.charCodeAt(0) + 13;

    return String.fromCharCode((c <= 'Z' ? 90 : 122) >= cplus ? cplus : cplus - 26);
  });

  return str;
};

// Convert string to Java-like hash code
// Based on http://werxltd.com/wp/2010/05/13/javascript-implementation-of-javas-string-hashcode-method/
export let hashCode = function hashCode(str, prefix) {
  var hash = 0;
  var i, chr, len;

  if (str.length === 0) {
    return hash;
  }

  for (i = 0, len = str.length; i < len; i++) {
    chr   = str.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    // Convert to 32bit integer
    hash |= 0;
  }

  hash = hash >= 0 ? hash : hash * -1;

  if (prefix) {
    hash = prefix + hash;
  }

  return hash;
};

export let base64Encode = function base64Encode(str) {
  if (!btoa) {
    return str;
  }

  return btoa(encodeURIComponent(str));
};

export let base64Decode = function base64Decode(str) {
  if (!atob) {
    return str;
  }

  return decodeURIComponent(atob(str));
};
