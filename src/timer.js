export let debounce = function debounce(fn, delay, ctx) {
  var timeout;

  delay = delay === undefined ? 200 : delay;

  return function() {
    var args = [].slice.call(arguments);

    ctx = ctx || this;

    clearTimeout(timeout);
    timeout = setTimeout(function() {
      fn.apply(ctx, args);
      timeout = ctx = args = null;
    }, delay);
  };
};
// Same as debounce, but triggers once at the beginning
export let unbounce = function unbounce(fn, delay, ctx) {
  var timeout;

  delay = delay === undefined ? 200 : delay;

  return function() {
    var args = [].slice.call(arguments);

    ctx = ctx || this;

    if (!timeout) {
      fn.apply(ctx, args);
    }

    timeout = setTimeout(function() {
      timeout = ctx = args = null;
    }, delay);
  };
};
export let throttle = function throttle(fn, delay, ctx) {
  var previous = 0;

  delay = delay === undefined ? 200 : delay;

  return function() {
    var remaining;
    var args = [].slice.call(arguments);
    var first = previous === 0;
    var now = +new Date();

    previous = previous || now;
    ctx = ctx || this;
    remaining = delay - (now - previous);

    if (first || remaining <= 0 || remaining > delay) {
      previous = now;
      fn.apply(ctx, args);
      ctx = null;
    }
  };
};
