// Touch events
(function() {

var e, holdTimer;
var moved = 0;
var held = false;

var startPosition = {
  x: 0,
  y: 0
};
var endPosition = {
  x: 0,
  y: 0
};

var createEvt = function(event, name) {
  var customEvent = document.createEvent('CustomEvent');

  customEvent.initCustomEvent(name, true, true, event.target);
  event.target.dispatchEvent(customEvent);
  customEvent = null;

  return false;
};

var touch = {
  touchstart: function(event) {
    clearTimeout(holdTimer);
    startPosition = {
      x: event.touches[0].pageX,
      y: event.touches[0].pageY
    };
    holdTimer = setTimeout(function() {
      if (!moved) {
        event.preventDefault();
        createEvt(event, 'taphold');
        held = true;
      }
    }, 400);
  },

  touchmove: function(event) {
    moved++;
    endPosition = {
      x: event.touches[0].pageX,
      y: event.touches[0].pageY
    };
  },

  touchend: function(event) {
    var gesture, xDelta, xDeltaAbs, yDelta, yDeltaAbs;
    var threshold = window.FM && window.FM.touchThreshold || 20;

    clearTimeout(holdTimer);

    if (!held) {
      if (!moved) {
        createEvt(event, 'tap');
      } else {
        xDelta = endPosition.x - startPosition.x;
        xDeltaAbs = Math.abs(xDelta);
        yDelta = endPosition.y - startPosition.y;
        yDeltaAbs = Math.abs(yDelta);

        if (Math.max(xDeltaAbs, yDeltaAbs) > threshold) {
          if (xDeltaAbs > yDeltaAbs) {
            gesture = xDelta < 0 ? 'swipeleft' : 'swiperight';
          } else {
            gesture = yDelta < 0 ? 'swipeup' : 'swipedown';
          }
          event.preventDefault();
          createEvt(event, gesture);
        }
      }
    }

    held = false;
    moved = 0;
  },

  touchcancel: function() {
    held = false;
    moved = 0;
  }
};

for (e in touch) {
  document.addEventListener(e, touch[e], false);
}
})();
