// URL functions
import {extend} from './core';

let getObjectType = function getObjectType(obj) {
  var type = Object.prototype.toString.call(obj);
  var ret = '';

  if (type === '[object Array]') {
    ret = 'array';
  } else if (type === '[object Object]') {
    ret = 'object';
  }

  return ret;
};

const buildParams = function buildParams(prefix, obj, options, add) {
  var val, valType;
  var objType = getObjectType(obj);
  var l = obj && obj.length;

  if (objType === 'array') {
    for (var i = 0; i < l; i++) {
      // Serialize array item.
      val = obj[i];
      valType = getObjectType(val);
      buildParams(`${prefix}[${valType || options.indexed ? i : ''}]`, val, options, add);
    }

  } else if (objType === 'object') {
    // Serialize object item.
    for (var name in obj) {
      buildParams(`${prefix}[${name}]`, obj[ name ], options, add);
    }

  } else {
    // Serialize scalar item.
    if (typeof obj === 'undefined') {
      obj = '';
    }
    add(prefix, obj);
  }
};

var r20 = /%20/g;
var rPlus = /\+/g;

// normalize pathname (old IE doesn't include initial "/" for this.pathname)
export let pathname = function(el) {
  el = el || location;
  var pathParts = [];
  var path = el.pathname || '';

  if (typeof el === 'string') {
    // convert any URL-y string to a pathname
    if (el.indexOf('//') === 0) {
      el = location.protocol + el;
    }
    // remove "#..." and "?..."
    path = el.replace(/#.*$/, '').replace(/\?.*$/, '');
    // remove protocol, domain, etc.
    if (/^https?:\/\//.test(path)) {
      pathParts = path.split(/\//).slice(3);
      path = pathParts.join('/');
    }
  }

  path = `/${path.replace(/^\//, '')}`;

  return path;
};

// Get basename of a location or string
export let basename = function(el, ext) {
  var rExt;
  var path = pathname(el).split(/\//).pop() || '';

  if (ext) {
    ext = ext.replace(/\./g, '\\.');
    rExt = new RegExp(`${ext}$`);
    path = path.replace(rExt, '');
  }

  return path;
};

export let segments = function segments(path) {
  path = pathname(path).replace(/^\/|\/$/g, '');

  return path.split('/') || [];
};

export let segment = function segment(index, path) {
  let segs = segments(path);
  let seg = segs[index] || '';

  if (index < 0) {
    index *= -1;

    // Avoid ridiculously large number for index grinding things to a halt
    index = Math.min(index, segs.length + 1);

    while (index-- > 0) {
      seg = segs.pop() || '';
    }
  }

  return seg;
};

export let loc = function loc(el) {
  let locat = {
    pathname: pathname(el),
    basename: basename(el)
  };
  let href, segment, host, protocol;
  let hrefParts = ['host', 'pathname', 'search', 'hash'];
  let parts = {hash: '#', search: '?'};

  if (typeof el === 'string') {

    for (var part in parts) {
      segment = el.split(parts[part]);
      locat[part] = '';

      if (segment.length === 2 && segment[1].length) {
        locat[part] = parts[part] + segment[1];
        el = segment[0];
      }
    }

    protocol = el.split(/\/\//);
    locat.protocol = protocol.length === 2 ? protocol[0] : '';
    el = protocol.pop();
    locat.host = el === locat.pathname ? location && location.host || '' : el.split('/')[0];
    host = locat.host.split(':');
    locat.hostname = host[0];
    locat.port = host.length > 1 ? host[1] : '';

    href = `${locat.protocol || 'http:'}//`;

    for (var i = 0; i < hrefParts.length; i++) {
      href += locat[ hrefParts[i] ];
    }
    locat.href = href;
  } else {
    el = el || {};

    for (let key in el) {
      if (typeof locat[key] === 'undefined') {
        locat[key] = el[key];
      }
    }
  }

  return locat;
};

// Remove potentially harmful characters from hash and escape dots
export let hashSanitize = function hashSanitize(hash) {
  hash = hash || '';

  return hash.replace(/[^#_\-\w\d\.\!\/]/g, '').replace(/\./g, '\\.');
};

// Convert an object to a serialized string
// options: raw, prefix, indexed
export let serialize = function serialize(data, options) {
  options = options || {};
  let obj = {};
  var serial = [];
  var add = function(key, value) {
    var item = options.raw ? value : encodeURIComponent(value);

    serial[ serial.length ] = `${key}=${item}`;
  };

  if (options.prefix) {
    obj[options.prefix] = data;
  } else {
    obj = data;
  }

  // If options.prefix is set, assume we want arrays to have indexed notation (foo[0]=won)
  // Unless options.indexed is explicitly set to false
  options.indexed = options.indexed || (options.prefix && options.indexed !== false);

  if (getObjectType(obj)) {
    for (var prefix in obj) {
      buildParams(prefix, obj[prefix], options, add);
    }
  }

  return serial.join('&').replace(r20, '+');
};

let getParamObject = function getParamObject(param, opts) {
  let paramParts = param.split('=');
  let key = opts.raw ? paramParts[0] : decodeURIComponent(paramParts[0]);
  let val;

  if (paramParts.length === 2) {
    // First replace all '+' characters with ' '; then decode it
    val = opts.raw ? paramParts[1] : decodeURIComponent(paramParts[1].replace(rPlus, ' '));
  } else {
    val = opts.empty;
  }

  return {key, val};
};

// Convert a serialized string to an object

export let unserialize = function unserialize(string, options) {

  if (typeof string === 'object') {
    options = string;
    string = location && location.search || '';
  } else {
    string = string || location && location.search || '';
  }

  var opts = extend({
    // if true, param values will NOT be urldecoded
    raw: false,
    // the value of param with no value (e.g. ?foo&bar&baz )
    // typically, this would be either true or ''
    empty: true,
    // if true, does not attempt to build nested object
    shallow: false,
  }, options || {});

  string = string.replace(/^\?/, '');

  var key, keyParts, keyRoot, keyEnd, val;
  var obj = {};
  // var hasBrackets = /^(.+)(\[([^\]]*)\])+$/;
  var rIsArray = /\[\]$/;
  var params = [];
  var paramParts = [];

  if (!string) {
    return obj;
  }

  params = string.split(/&/);

  for (var i = 0, l = params.length; i < l; i++) {
    let {key, val} = getParamObject(params[i], opts);

    // Set shallow key/val pair
    if (opts.shallow) {
      if (rIsArray.test(key)) {
        key = key.replace(rIsArray, '');
        obj[key] = obj[key] || [];
        obj[key].push(val);
      } else {
        obj[key] = val;
      }

      continue;
    }


    // TODO: Make the rest of this function more DRY
    // Split on brackets
    keyParts = key.replace(/\]/g, '').split('[');

    // The first element of the array is always the "root" so shift it off
    keyRoot = keyParts.shift();

    // If nothing left after root, it's just a string
    if (!keyParts.length) {
      obj[keyRoot] = val;
      continue;
    }

    // Now, if we still have parts of the key, we're dealing with an array or object
    keyEnd = keyParts.pop();

    // single-level array/obj
    if (!keyParts.length) {
      // array
      if (keyEnd === '') {
        obj[keyRoot] = obj[keyRoot] ? obj[keyRoot].concat(val) : [val];
      } else {
        obj[keyRoot] = obj[keyRoot] || {};
        obj[keyRoot][keyEnd] = val;
      }
      continue;
    }

    // nested obj -> array
    obj[keyRoot] = obj[keyRoot] || {};
    obj[keyRoot][ keyParts[0] ] = obj[keyRoot][ keyParts[0] ] ? obj[keyRoot][ keyParts[0] ].concat(val) : [val];
  }

  return obj;
};
