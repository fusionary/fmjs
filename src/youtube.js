import {unserialize} from './url';
// get the ID of a youtube video from an element or its href or its src
export let getYoutubeId = function getYoutubeId(link) {
  var id = '';
  var params = {};
  var href = typeof link === 'string' ? link : (link.href || link.src);
  var hrefParts = href.split('?');

  if (hrefParts.length === 2) {
    params = unserialize(hrefParts[1]);
    id = params.v || params.embed;
  }

  if (!id) {
    id = hrefParts[0].split(/\//).pop();
  }

  return id || '';
};
