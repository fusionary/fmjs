
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([<%= dependencies %>], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(require(<%= dependencies %>));
  } else {
    root.returnExports = factory(root.FM);
  }
}(this, function() {
