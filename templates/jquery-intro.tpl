(function(factory) {
  if (typeof define === 'function' && define.amd) {
    define([<%= dependencies %>], factory);
  } else if (typeof exports === 'object') {
    factory(require(<%= dependencies %>));
  } else {
    factory(jQuery);
  }
}(function($) {
