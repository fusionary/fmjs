/* jshint -W008 */
/* global QUnit: false */
'use strict';
import {extend, getProperty} from '../src/core';
import * as arrays from '../src/array';
import * as maths from '../src/math';
import * as strings from '../src/string';
import * as urls from '../src/url';
import * as events from '../src/event';
import * as addElements from '../src/addelement';
import * as youtubes from '../src/youtube';
import * as detects from '../src/transition-event';
import * as selections from '../src/selection';
import {ajax, asyncBrowserOnly} from './ajax-fake';
import qs from 'qs';
import {Storage} from '../src/storage.js';

// TODO: TESTS FOR THE FOLLOWING…
// jsonp.js: getJSONP
// selection.js: replaceSelection/wrapSelection
// string.js: pluralize
// timer.js: throttle, debounce

QUnit.module('core', {
  setup: function() {
    this.target = {
      foo: {
        bar: {
          baz: 'Hello, I am a baz'
        },
        flotsam: ['Meet George Jetsam', 'Jude, his wife']
      },
      cb: function() {
        return 'hello';
      }
    };

    this.targetArray = [
      {
        foo: 'bar',
        name: {
          first: 'karl',
          last: 'swedberg'
        },
        cb: function() {
          return 'hello';
        }
      }
    ];
  }
});

QUnit.test('extend: array of objects', function() {

  var b = FM.extend([], this.targetArray);

  this.targetArray[0].name.first = 'dean';

  QUnit.expect(3);

  QUnit.equal(b[0].foo, 'bar', 'Extend copies property');
  QUnit.equal(b[0].name.first, 'karl', 'Extend does not copy by reference');
  QUnit.equal(b[0].cb(), 'hello', 'Extend copies function');
});

QUnit.test('extend: object into empty', function() {

  var a = this.target;

  var b = extend({}, a);

  a.foo.flotsam[0] = 'Meet me in the middle';

  QUnit.expect(3);

  QUnit.equal(b.foo.bar.baz, 'Hello, I am a baz', 'Extend copies nested property');
  QUnit.equal(b.foo.flotsam[0], 'Meet George Jetsam', 'Extend does not copy by reference');
  QUnit.equal(b.cb(), 'hello', 'Extend copies function');
});

QUnit.test('extend: 2 objects into target', function() {

  var a = this.target;

  var b = {
    foo: {
      bar: {
        baz: 'I am Baz, the Magnificent!'
      }
    },
    baz: 'nope',
    name: {
      first: 'tim',
      middle: 'johann'
    }
  };

  var c = {
    baz: 'yep'
  };

  extend(a, b, c);

  QUnit.expect(4);
  QUnit.equal(a.baz, 'yep', 'Adds property');
  QUnit.equal(a.foo.bar.baz, 'I am Baz, the Magnificent!', 'Overwrites nested property');
  QUnit.equal(a.name.middle, 'johann', 'Adds nested property');

  a.baz = 'diff';
  QUnit.equal(b.baz, 'nope', 'source object keeps prop when target prop changes');
});

QUnit.test('getProperty', function() {
  var stringStyle = getProperty(this.target, 'foo.bar.baz');
  var arrayStyle = getProperty(this.target, ['foo', 'bar', 'baz']);
  var undef = getProperty(this.target, ['foo', 'bar', 'nothere']);
  var undefEarly = getProperty(this.target, ['nothere', 'bar', 'baz']);
  var undefRoot = getProperty(window.foo, 'foo.bar');

  QUnit.expect(6);
  QUnit.equal(typeof FM.getProperty, 'function', 'getProperty was added to global FM object');
  QUnit.equal(stringStyle, 'Hello, I am a baz', 'this.target.foo.bar.baz == "Hello", i am a baz');
  QUnit.equal(arrayStyle, 'Hello, I am a baz', 'this.target.foo.bar.baz == "Hello", i am a baz');
  QUnit.equal(undef, undefined, 'this.target.foo.bar.nothere == undefined');
  QUnit.equal(undefEarly, undefined, 'this.target.nothere.bar.baz == undefined');
  QUnit.equal(undefRoot, undefined, 'root param is undefined');
});

QUnit.module('arrays');

QUnit.test('isArray', function() {
  QUnit.expect(2);

  QUnit.ok(arrays.isArray([1,2,4]), '[1,2,4] is an array');
  QUnit.ok(!arrays.isArray({0: 'one', length: 1}), '{0: "one", length: 1} is NOT an array');
});

QUnit.test('inArray', function() {
  QUnit.expect(2);

  QUnit.ok(arrays.inArray(3, [3,2,1]), '3 is in array [3, 2, 1]');
  QUnit.ok(!arrays.inArray('2', [3,2,1]), '"2" is NOT in array [3, 2, 1]');
});

QUnit.test('inArray', function() {
  QUnit.expect(5);

  var arr = [3,2,1];

  QUnit.ok(arrays.inArray(arrays.randomItem(arr), arr), 'Random item is in array [3, 2, 1]');
  QUnit.ok(arrays.inArray(arrays.randomItem(arr), arr), 'Random item is in array [3, 2, 1]');
  QUnit.ok(arrays.inArray(arrays.randomItem(arr), arr), 'Random item is in array [3, 2, 1]');
  QUnit.ok(arrays.inArray(arrays.randomItem(arr), arr), 'Random item is in array [3, 2, 1]');
  QUnit.ok(arrays.inArray(arrays.randomItem(arr), arr), 'Random item is in array [3, 2, 1]');
});

QUnit.test('pluck', function() {
  var family = [
    {
      name: 'Karl',
      color: 'orange'
    },
    {
      name: 'Sara',
      color: 'yellow'
    },
    {
      name: 'Ben',
      color: 'blue'
    },
    {
      name: 'Lucy',
      color: 'green'
    }
  ];
  var names = arrays.pluck(family, 'name');
  var colors = arrays.pluck(family, 'color');

  QUnit.deepEqual(names, ['Karl', 'Sara', 'Ben', 'Lucy'], 'Plucked names out of family');
  QUnit.deepEqual(colors, ['orange', 'yellow', 'blue', 'green'], 'Plucked colors out of family');
});

QUnit.module('math');

QUnit.test('add', function() {
  QUnit.equal(maths.add([3, 4]), 7, 'Simple addition');
  QUnit.equal(maths.add([3, 4, 6]), 13, 'Add three numbers');
  QUnit.equal(maths.add([3, 4, 6, 4], 3), 20, 'Add four numbers, starting with 3');
});

// This is just to make sure the "functional" nature of fm.math.js is working as expected.
// If this works, then fm.divide() and fm.multiply() will work, as well
QUnit.test('subtract', function() {
  QUnit.equal(maths.subtract([20, 4]), -24, 'Simple subtraction');
  QUnit.equal(maths.subtract([30, 4, 6]), -40, 'Subtract three numbers');
  QUnit.equal(maths.subtract([3, 4, 6, 4], 40), 23, 'Subtract four numbers, starting with 40');
});

QUnit.test('modulo', function() {
  QUnit.equal(maths.mod([4, 3]), 1, 'Get remainder (mod) of array of two numbers');
  QUnit.equal(maths.mod(85, 60), 25, 'Get remainder (mod) of two numbers');
});

QUnit.module('string');

QUnit.test('formatNumber', function() {
  QUnit.equal(strings.formatNumber(3.2), '3.2', 'Stupid simple number to string.');
  QUnit.equal(strings.formatNumber(3.2, {
    decimalPlaces: 2
  }), '3.20', 'Number up to fixed 2 decimal places.');
  QUnit.equal(strings.formatNumber(3.248887532, {
    decimalPlaces: 2
  }), '3.25', 'Number down to fixed 2 decimal places.');
  QUnit.equal(strings.formatNumber(32.047000000000004, {
    decimalPlaces: 2
  }), '32.05', 'Number down to left-padded, fixed 2 decimal places.');

  QUnit.equal(strings.formatNumber(.93432, {
    decimalPlaces: 1,
    zeroPad: 1
  }), '0.9', 'Number down to fixed 1 decimal place with leading 0.');

  QUnit.equal(strings.formatNumber(2, {
    prefix: '$',
    includeHtml: false,
    decimalPlaces: 2

  }), '$2.00', 'No cents to dollars and cents');

  QUnit.equal(strings.formatNumber(2.5555, {
    prefix: '$',
    includeHtml: false,
    decimalPlaces: 2

  }), '$2.56', 'Dollars and cents');

  QUnit.equal(strings.formatNumber(5 / 3, {
    suffix: ' USD$',
    includeHtml: false,
    decimalPlaces: 2

  }), '1.67 USD$', 'USD$');

  var htmlFormatted = '<span class="Price"><span class="Price-prefix">$ </span><span class="Price-int">1</span><span class="Price-dec">.</span><span class="Price-decNum">67</span></span>';

  QUnit.equal(strings.formatNumber(5 / 3, {
    prefix: '$ ',
    decimalPlaces: 2

  }), htmlFormatted, 'HTML');

});

QUnit.test('hashCode', function() {
  QUnit.equal(strings.hashCode('a'), 97, 'Converts string to numeric hashCode');
  QUnit.equal(strings.hashCode('a*a'), 94616, 'Converts string to numeric hashCode');
});

QUnit.test('rot13', function() {
  var rot13 = {
    original: 'Karl Swedberg',
    expect: 'Xney Fjrqoret'
  };

  rot13.encoded = strings.rot13(rot13.original);
  rot13.decoded = strings.rot13(rot13.encoded);

  QUnit.equal(rot13.encoded, rot13.expect, `${rot13.original} is rot13 encoded to ${rot13.encoded}`);
  QUnit.equal(rot13.original, rot13.decoded, 'String is base64 encoded, then decoded to original value');

});

QUnit.test('base64', function() {

  var base64 = {
    original: 'Hello there, how are you?'
  };

  base64.encoded = strings.base64Encode(base64.original);
  base64.decoded = strings.base64Decode(base64.encoded);

  QUnit.notEqual(base64.original, base64.encoded, 'String is base64 encoded');
  QUnit.equal(base64.original, base64.decoded, 'String is base64 encoded, then decoded to original value');
});

QUnit.test('slugify', function() {

  var slugs = [
    {
      pre: 'Hello there, how are you?',
      expected: 'hello-there-how-are-you'
    },
    {
      pre: '  You? & Me<3* ',
      expected: 'you-me-3'
    }
  ];

  slugs.forEach(function(item) {
    var slugged = strings.slugify(item.pre);

    QUnit.equal(slugged, item.expected, `${item.pre} is slugified to ${slugged}`);
  });
});

QUnit.module('url');

QUnit.test('location', function() {

  var loc1 = 'http://www.example.com/path/to/file/?skippy=jif#foooooo';
  var expect1 = {
    href: 'http://www.example.com/path/to/file/?skippy=jif#foooooo',
    protocol: 'http:',
    host: 'www.example.com',
    hostname: 'www.example.com',
    port: '',
    pathname: '/path/to/file/',
    basename: '',
    search: '?skippy=jif',
    hash: '#foooooo'
  };
  var loc2 = '//example.com/path/to/file#foooooo';
  var expect2 = {
    href: 'http://example.com/path/to/file#foooooo',
    protocol: '',
    host: 'example.com',
    hostname: 'example.com',
    port: '',
    pathname: '/path/to/file',
    basename: 'file',
    search: '',
    hash: '#foooooo'
  };

  var loc3 = '/path/to/file';
  var expect3 = {
    href: 'http://localhost:8001/path/to/file',
    protocol: '',
    host: 'localhost:8001',
    hostname: 'localhost',
    port: '8001',
    pathname: '/path/to/file',
    basename: 'file',
    search: '',
    hash: ''
  };
  var loc4 = {};

  ['pathname', 'host', 'hostname', 'protocol', 'port', 'search', 'hash'].forEach(function(part) {
    loc4[part] = location[part];
  });
  var expect4 = {
    pathname: '/test/index.html',
    host: 'localhost:8001',
    hostname: 'localhost',
    basename: 'index.html',
    port: '8001',
    protocol: 'http:',
    search: '',
    hash: ''
  };

  QUnit.deepEqual(urls.loc(loc1), expect1, '1 from full href, no basename');
  QUnit.deepEqual(urls.loc(loc2), expect2, '2 from fully href, no hash or search');

  if (location.hostname === 'localhost') {
    // Test 3 and 4 only if running from command line (grunt test)
    QUnit.deepEqual(urls.loc(loc3), expect3, '3 from pathname only');
    QUnit.deepEqual(urls.loc(loc4), expect4, '4 location parts for window.location');
  }
});

QUnit.test('pathname', function() {
  var path1 = location;
  var path2 = 'http://www.example.com/path/to/file/?skippy=jif#foooooo';
  var path3 = '//www.example.com/path/to/file/yummy.html?skippy=jif#foooooo';
  var path4 = 'https://www.example.com/path/to/file/?skippy=jif#foooooo';

  QUnit.equal(urls.pathname(path1), '/test/index.html', 'pathname from location');
  QUnit.equal(urls.pathname(path2), '/path/to/file/', 'pathname from full href string');
  QUnit.equal(urls.pathname(path3), '/path/to/file/yummy.html', 'pathname from protocol-relative href string');
  QUnit.equal(urls.pathname(path4), '/path/to/file/', 'pathname from https href string');
});

QUnit.test('segments', function() {
  var path2 = 'http://www.example.com/path/to/file/?skippy=jif#foooooo';

  QUnit.deepEqual(urls.segments(), ['test', 'index.html'], 'segments from location.pathname');
  QUnit.deepEqual(urls.segments(path2), ['path', 'to', 'file'], 'segments from full href string and trailing slash');
});
QUnit.test('segment', function() {
  var path1 = 'http://www.example.com/path/to/file/?skippy=jif#foooooo';

  QUnit.equal(urls.segment(0), 'test', 'first segment from location.pathname');
  QUnit.equal(urls.segment(1), 'index.html', '2nd segment from location.pathname');
  QUnit.equal(urls.segment(2), '', '3rd segment from location.pathname');
  QUnit.equal(urls.segment(-1), 'index.html', 'last segment from location.pathname');
  QUnit.equal(urls.segment(-2), 'test', 'penultimate segment from location.pathname');
  QUnit.equal(urls.segment(-59), '', 'non-existent segment from location.pathname');
  QUnit.equal(urls.segment(1, path1), 'to', '2nd segment from full href string and trailing slash');
  QUnit.equal(urls.segment(-1, path1), 'file', 'last segment from full href string and trailing slash');
});
QUnit.test('basename', function() {
  var path1 = location;
  var path2 = 'http://www.example.com/path/to/file/?skippy=jif#foooooo';
  var path3 = 'http://www.example.com/path/to/file?skippy=jif#foooooo';
  var path4 = '//www.example.com/path/to/file/yummy.html?skippy=jif#foooooo';
  var path5 = 'https://www.example.com/path/to/file/yummy.html?skippy=jif#foooooo';
  var path6 = '/path/to/file.foo.js';

  QUnit.equal(urls.basename(path1), 'index.html', 'basename from location');
  QUnit.equal(urls.basename(path2), '', 'basename from full href string');
  QUnit.equal(urls.basename(path3), 'file', 'basename from full href string, no trailing slash');
  QUnit.equal(urls.basename(path4), 'yummy.html', 'basename from protocol-relative href string');

  QUnit.equal(urls.basename(path5, '.html'), 'yummy', 'basename from href string, stripping .html extension');
  QUnit.equal(urls.basename(path6, '.js'), 'file.foo', 'basename from pathname string, stripping .js extension but preserving rest of file name with dot');
});

QUnit.test('hashSanitize', function() {
  var hash1 = '#foo.bar';
  var hash2 = '#foo<script>alert("sucka!")</script>';

  QUnit.equal(urls.hashSanitize(hash1), '#foo\\.bar', 'escaped dot in hash');
  QUnit.equal(urls.hashSanitize(hash2), '#fooscriptalertsucka!/script', 'removed injected script');
});

QUnit.test('unserialize', function() {
  var qs1 = '?foo=bar&yoyo&baz=xyxxy';
  var qs2 = 'foo[]=1&foo[]=2';
  var qs3 = 'foo[bar]=baz&foo[yummy]=food';
  var qs4 = 'foo[bar][]=baz&foo[bar][]=food';
  var qs5 = 'foo[bar][baz][]=food';

  QUnit.deepEqual(urls.unserialize(qs1), {foo: 'bar', yoyo: true, baz: 'xyxxy'}, 'unserialized correctly');
  QUnit.deepEqual(urls.unserialize(qs1, {empty: ''}), {foo: 'bar', yoyo: '', baz: 'xyxxy'}, 'unserialized correctly, with empty option');
  QUnit.deepEqual(urls.unserialize(qs2), {foo: ['1', '2']}, 'unserialized correctly, with array param');
  QUnit.deepEqual(urls.unserialize(qs3), {foo: {bar: 'baz', yummy: 'food'}}, 'unserialized correctly, with obj param');
  QUnit.deepEqual(urls.unserialize(qs4), {foo: {bar: ['baz', 'food']}}, 'unserialized correctly, with nested array param');
  QUnit.deepEqual(urls.unserialize(qs3, {shallow: true}), {'foo[bar]': 'baz', 'foo[yummy]': 'food'}, 'shallow unserialized correctly');
  // QUnit.deepEqual(qs.parse(qs5), {foo: {bar: {'baz': ['food']}}}, 'unserialized correctly, with double-nested array param');
});

QUnit.test('serialize', function() {
  var obj1 = {
    foo: 'yes',
    bar: 'again'
  };
  var obj2 = {
    foo: ['yes', 'again']
  };
  var obj3 = {
    foo: {
      bar: ['one', 'two']
    }
  };
  var obj4 = {
    foo: {
      bar: 'yes I can'
    }
  };
  var obj5 = {
    foo: undefined,
    bar: '',
    baz: 0,
    yum: null
  };

  var array1 = [
    'oh',
    'won',
    'too'
  ];

  var arrayOpts = {prefix: 'foo'};

  QUnit.equal(urls.serialize(obj1), 'foo=yes&bar=again', 'serialized string');
  QUnit.equal(urls.serialize(obj2), 'foo[]=yes&foo[]=again', 'serialized array');
  QUnit.equal(urls.serialize(obj3), 'foo[bar][]=one&foo[bar][]=two', 'serialized object with nested array');
  QUnit.equal(urls.serialize(obj4), 'foo[bar]=yes+I+can', 'serialized object');
  QUnit.equal(urls.serialize(obj4, {raw: true}), 'foo[bar]=yes I can', 'serialized object, NOT urlencoded (raw)');
  QUnit.equal(urls.serialize(obj5), 'foo=&bar=&baz=0&yum=null', 'serialized string with falsy values');
  QUnit.equal(urls.serialize(array1, arrayOpts), 'foo[0]=oh&foo[1]=won&foo[2]=too', 'serialized string from array with prefix option');
});

QUnit.module('youtube');
QUnit.test('youtube id', function() {
  QUnit.expect(5);
  var hrefs = [
    'http://youtu.be/Dd7FixvoKBw',
    'https://www.youtube.com/watch?v=Dd7FixvoKBw',
    '//www.youtube.com/embed/Dd7FixvoKBw',
    '//www.youtube.com/v/Dd7FixvoKBw?version=3&amp;hl=en_US',
    'http://youtu.be/Dd7FixvoKBw?t=2s'
  ];

  for (var i = 0; i < hrefs.length; i++) {
    QUnit.equal(youtubes.getYoutubeId(hrefs[i]), 'Dd7FixvoKBw', 'Retrieved YouTube ID');
  }

});

QUnit.module('addelement');

QUnit.asyncTest('addScript', function() {
  QUnit.expect(1);
  addElements.addScript('stub.js', 'stub', function() {
    QUnit.ok(FM.stubbed, 'stub.js loaded and executed');
    QUnit.start();
  });

});

QUnit.module('event');

QUnit.asyncTest('addEvent', function() {

  QUnit.expect(2);

  events.addEvent(window, 'load', function() {
    QUnit.ok(FM.windowLoaded, 'window loaded');
    events.addEvent(window, 'load', function() {
      QUnit.ok(FM.windowLoaded, 'window load triggered immediately when called after window loaded');
      QUnit.start();
    });
  });
});

QUnit.module('ajax');

QUnit.asyncTest('ajax', function() {
  QUnit.expect(1);

  ajax('stub.json', {dataType: 'json'})
  .then(function(result) {
    QUnit.equal(result.response.status, 'success', 'fetched and parsed json');
    QUnit.start();
  })
  .catch(function(xhr) {
    console.log(JSON.stringify(xhr, null, 2));
    QUnit.start();
  });

});
QUnit.asyncTest('ajax.getJson', asyncBrowserOnly(function() {
  QUnit.expect(1);
  ajax.getJson('stub.json', {cache: false})
  .then(function(result) {
    QUnit.equal(result.response.status, 'success', 'fetched and parsed json');
    QUnit.start();
  })
  .catch(function(xhr) {
    console.log(JSON.stringify(xhr, null, 2));
    QUnit.start();
  });
}));

QUnit.asyncTest('ajax.getJson memcached', asyncBrowserOnly(function() {
  QUnit.expect(1);

  ajax.getJson('stub.json', {memcache: true})
  .then((first) => {
    return ajax.getJson('stub.json', {memcache: true})
    .then((second) => {
      QUnit.equal(first.timestamp, second.timestamp, 'memcached second request same as first');
      QUnit.start();
    })
    .catch(function(xhr) {
      console.log(JSON.stringify(xhr, null, 2));
      QUnit.start();
    });
  });
}));

QUnit.asyncTest('ajax.getJson not memcached', asyncBrowserOnly(function() {
  QUnit.expect(1);

  ajax.getJson('stub.json')
  .then((first) => {

    ajax.getJson('stub.json')
    .then((second) => {
      QUnit.notEqual(first.timestamp, second.timestamp, 'Non-memcached second request different from first');
      QUnit.start();
    })
    .catch(function(xhr) {
      console.log(JSON.stringify(xhr, null, 2));
      QUnit.start();
    });
  });
}));

QUnit.module('storage', {
  setup: function() {
    this.storage = new Storage(null, 'fm-');
    localStorage.setItem('dummy', 'hello');
    this.storage.set('item', {foo: 'bar', welp: 'yippee', yay: {deep: 'yes', shallow: 'no'}});
    this.storage.set('item2', {bar: 'baz'});
  },
  teardown: function() {
    localStorage.clear();
  }
});

QUnit.test('storage.get()', function() {
  var item = this.storage.get('item');

  QUnit.equal(item.foo, 'bar', 'storage item prop was set');
  QUnit.equal(this.storage.length, 2, 'correctly indicates length of namespaced items');

});
QUnit.test('storage.keys()', function() {
  let items = this.storage.keys();

  QUnit.equal(items[0], 'item', 'first item key');
  QUnit.equal(items[1], 'item2', 'second item key');
});

QUnit.test('storage.getAll()', function() {
  let items = this.storage.getAll();

  QUnit.equal(items.item.foo, 'bar', 'first item prop');
  QUnit.equal(items.item2.bar, 'baz', 'second item prop');
});

QUnit.test('storage.toArray()', function() {
  let items = this.storage.toArray();

  QUnit.equal(items[0].key, 'item', 'first item key');
  QUnit.equal(items[1].key, 'item2', 'second item key');
  QUnit.equal(items[0].foo, 'bar', 'first item prop');
  QUnit.equal(items[1].bar, 'baz', 'second item prop');
});

QUnit.test('storage.map()', function() {
  let items = this.storage.map(function(key, val) {
    val.keey = key;

    return val;
  });

  QUnit.equal(items.item.keey, 'item', 'first item key');
  QUnit.equal(items.item2.keey, 'item2', 'second item key');
});

QUnit.test('storage.filter()', function() {
  let items = this.storage.filter(function(key, val) {
    return key === 'item';
  });

  QUnit.equal(items.item.foo, 'bar', 'first item foo');
  QUnit.equal(!!items.item2, false, 'second item filtered out');
});

QUnit.test('storage.filterToArray()', function() {
  let items = this.storage.filterToArray(function(key, val) {
    return key === 'item';
  });

  QUnit.equal(items[0].foo, 'bar', 'first item foo');
  QUnit.equal(items.length, 1, 'second item filtered out');
});

QUnit.test('storage.merge()', function() {
  let options = {
    hello: 'goodbye',
    welp: 'skippy',
    yay: {
      shallow: 'no way'
    }
  };

  let item = this.storage.merge(true, 'item', options);

  QUnit.equal(item.foo, 'bar', 'foo is bar');
  QUnit.equal(item.hello, 'goodbye', 'hello is goodbye');
  QUnit.equal(item.welp, 'skippy', 'welp is skippy');
  QUnit.equal(item.yay.deep, 'yes', 'yay.deep is still yes');
  QUnit.equal(item.yay.shallow, 'no way', 'yay.shallow is no way');
});

QUnit.test('storage.remove()', function() {
  this.storage.remove('item2');

  QUnit.equal(!!this.storage.get('item2'), false, 'item2 removed');
  QUnit.equal(this.storage.length, 1, 'length updated');
});

QUnit.test('storage.clear()', function() {
  this.storage.clear();

  QUnit.equal(!!this.storage.get('item'), false, 'item removed');
  QUnit.equal(this.storage.length, 0, 'all items removed');
  QUnit.equal(localStorage.getItem('dummy'), 'hello', 'dummy item preserved');
});

QUnit.module('selection');

QUnit.test('set & get text selection', function() {
  var selection, text, partial;
  var el = document.getElementById('qunit-fixture');

  selections.setSelection(el, 0);
  selection = selections.getSelection(el);

  text = selection.text.replace(/^\s+|\s+$/g, '');

  selections.setSelection(el, 5, 11);
  partial = selections.getSelection(el);
  QUnit.equal(text, 'test markup', '"test markup" was selected');
  QUnit.equal(partial.text, 'markup', 'partial "markup" was selected');
});

QUnit.test('set & get input selection', function() {
  var selection;
  var el = document.getElementById('select-input');

  selections.setSelection(el);
  selection = selections.getSelection(el);

  QUnit.equal(selection.text, 'test value', '"test markup" was selected');
});

QUnit.test('set & replace text selection', function() {
  var selection;
  var el = document.getElementById('qunit-fixture');

  selections.setSelection(el);
  selections.replaceSelection(el, 'hello there!');

  selection = selections.getSelection(el);

  QUnit.equal(selection.text, 'hello there!', 'selection was set and text replaced');
});

QUnit.module('transition-event');

QUnit.test('detect transition event', function() {
  QUnit.expect(2);
  var expectedAnimation = location.port === '8001' ? 'webkitAnimationEnd' : 'animationend';

  QUnit.equal(detects.transitionEnd, 'transitionend', 'detects.transitionEvent should equal transitionend');
  QUnit.equal(detects.animationEnd, expectedAnimation, 'detects.animationEnd should equal animationend');
});
