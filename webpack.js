// webpack config ***for Grunt***, so it looks a little different
var path = require('path');
var cwd = process.cwd();
var webpack = require('webpack');

var webpackModules = {
  loaders: [
    {
      loader: 'babel-loader',
      test: /\.jsx?$/,
      include: [
        /src\//,
        /test\//
      ],
    },
    {
      loader: 'json-loader',
      test: /\.json$/,
      include: [
        /test\//
      ]
    }
  ]
};

var definePlugin = new webpack.DefinePlugin({
  'process.env': {
    NODE_ENV: JSON.stringify('production')
  }
});

var minPlugins = [

  // Apparently some libs (React) may use process.env.NODE_ENV, so we better set it.
  // https://github.com/facebook/react/blob/master/docs/docs/getting-started.md#using-react-from-npm
  definePlugin,
  new webpack.optimize.DedupePlugin(),
  new webpack.optimize.UglifyJsPlugin({
    compress: {
      // eslint-disable-next-line camelcase
      drop_console: true,
      warnings: false
    }
  })
];

var nodetest = {
  entry: {
    test: [path.join(cwd, 'test/test.js')]
  },
  output: {
    filename: '[name].js',
    path: path.join(cwd, 'test/es6'),
  },
  module: webpackModules,
};

var browsertest = Object.assign({}, nodetest);

nodetest.plugins = [definePlugin];

module.exports = {
  nodetest,
  browsertest,
  testDist: {
    entry: {
      test: [path.join(cwd, 'test/dist.js')]
    },
    output: {
      filename: '[name].js',
      path: path.join(cwd, 'test/dist'),
    },
    module: webpackModules
  },

  dist: {
    entry: {
      fm: [path.join(cwd, 'src/fm.js')]
    },
    output: {
      filename: '[name].js',
      path: path.join(cwd, 'dist'),
    },
    module: webpackModules
  },
  min: {
    entry: {
      'fm.min': [path.join(cwd, 'src/fm.js')]
    },
    output: {
      filename: '[name].js',
      path: path.join(cwd, 'dist')
    },
    module: webpackModules,
    plugins: minPlugins
  }

};
